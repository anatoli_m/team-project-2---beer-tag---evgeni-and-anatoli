package com.telerik.beertag.services;

import com.telerik.beertag.exceptions.DuplicateEntityException;
import com.telerik.beertag.exceptions.EntityNotFoundException;
import com.telerik.beertag.models.Country;
import com.telerik.beertag.repositories.CountryRepositoryImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.telerik.beertag.Factory.createCountry;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class CountryServiceImplTests {

    @Mock
    CountryRepositoryImpl mockRepository;

    @InjectMocks
    CountryServiceImpl mockService;

    @Test
    public void createCountryShould_CallRepository() {
        //Arrange
        Country country = createCountry();

        //Act
        mockService.createCountry(country);

        //Assert
        Mockito.verify(mockRepository, times(1)).createCountry(country);
    }

    @Test
    public void createCountryShouldThrow_WhenCountryAlreadyExists() {
        //Arrange
        Mockito.when(mockRepository.checkIfCountryExists(anyString()))
                .thenReturn(true);

        //Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.createCountry(createCountry()));
    }

    @Test
    public void createCountryShould_ReturnNewCountry() {
        //Arrange
        Country country = createCountry();
        Mockito.when(mockRepository.createCountry(country)).thenReturn(country);

        //Act
        Country newCountry = mockService.createCountry(country);

        //Assert
        Assert.assertSame(country, newCountry);
    }

    @Test
    public void deleteCountryShould_CallRepository() {
        //Arrange
        Mockito.when(mockRepository.checkIfCountryExists(anyInt())).thenReturn(true);

        //Act
        mockService.deleteCountry(anyInt());

        //Assert
        Mockito.verify(mockRepository, times(1)).deleteCountry(anyInt());
    }

    @Test
    public void deleteCountryShouldThrow_WhenCountryDoesNotExists() {
        //Arrange
        Mockito.when(mockRepository.checkIfCountryExists(anyInt())).thenReturn(false);

        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockService.deleteCountry(anyInt()));
    }

    @Test
    public void deleteCountryShould_ReturnDeletedCountry() {
        //Arrange
        Country expectedCountry = createCountry();
        Mockito.when(mockRepository.checkIfCountryExists(anyInt())).thenReturn(true);
        Mockito.when(mockRepository.deleteCountry(anyInt())).thenReturn(expectedCountry);

        //Act
        Country deletedCountry = mockService.deleteCountry(expectedCountry.getId());

        //Assert
        Assert.assertSame(expectedCountry, deletedCountry);
    }

    @Test
    public void getCountryByIdShould_CallRepository() {
        //Arrange
        Mockito.when(mockRepository.checkIfCountryExists(anyInt())).thenReturn(true);

        //Act
        mockService.getCountryById(anyInt());

        //Assert
        Mockito.verify(mockRepository, times(1)).getCountryById(anyInt());
    }

    @Test
    public void getCountryByIdShouldThrow_WhenCountryDoesNotExists() {
        //Arrange
        Mockito.when(mockRepository.checkIfCountryExists(anyInt())).thenReturn(false);

        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockService.getCountryById(anyInt()));
    }

    @Test
    public void getCountryByIdShould_ReturnCountry() {
        //Arrange
        Country expectedCountry = createCountry();
        Mockito.when(mockRepository.checkIfCountryExists(anyInt())).thenReturn(true);
        Mockito.when(mockRepository.getCountryById(anyInt())).thenReturn(expectedCountry);

        //Act
        Country returnedCountry = mockService.getCountryById(anyInt());

        //Assert
        Assert.assertSame(expectedCountry, returnedCountry);
    }

    @Test
    public void updateCountryShould_CallRepository() {
        //Arrange
        Mockito.when(mockRepository.checkIfCountryExists(anyInt())).thenReturn(true);

        //Act
        mockService.updateCountry(createCountry());

        //Assert
        Mockito.verify(mockRepository, times(1))
                .updateCountry(any(Country.class));
    }

    @Test
    public void updateCountryShouldThrow_WhenCountryDoesNotExists() {
        //Arrange
        Mockito.when(mockRepository.checkIfCountryExists(anyInt())).thenReturn(false);

        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockService.updateCountry(createCountry()));
    }

    @Test
    public void updateCountryShould_ReturnUpdatedCountry() {
        //Arrange
        Country country = createCountry();
        Mockito.when(mockRepository.checkIfCountryExists(anyInt())).thenReturn(true);
        Mockito.when(mockRepository.updateCountry(any(Country.class)))
                .thenReturn(country);

        //Act
        Country updatedCountry = mockService.updateCountry(country);

        //Assert
        Assert.assertSame(country, updatedCountry);
    }
}
