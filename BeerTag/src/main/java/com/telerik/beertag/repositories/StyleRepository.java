package com.telerik.beertag.repositories;

import com.telerik.beertag.models.Style;

import java.util.List;

public interface StyleRepository {

    Style createStyle(Style style);

    Style updateStyle(int id, Style style);

    void deleteStyle(int id);

    List<Style> getAllStyles();

    Style getStyleById(int id);

    List<Style> getStyleByName(String name);

    boolean checkIfStyleExists(String style);

}
