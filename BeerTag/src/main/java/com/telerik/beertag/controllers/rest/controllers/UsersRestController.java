package com.telerik.beertag.controllers.rest.controllers;


import com.telerik.beertag.exceptions.DuplicateEntityException;
import com.telerik.beertag.exceptions.EntityNotFoundException;
import com.telerik.beertag.models.Beer;
import com.telerik.beertag.models.User;
import com.telerik.beertag.models.UserDto;
import com.telerik.beertag.services.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
//import org.graalvm.compiler.lir.LIRInstruction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;


@Api(description = "Operations pertaining to users in BeerTag")
@RestController
@RequestMapping("/api/users")
public class UsersRestController {

    private UserService service;

    @Autowired
    public UsersRestController(UserService service) {
        this.service = service;
    }

    @ApiOperation(value = "View a list of all users", response = List.class)
    @GetMapping
    public List<User> getAllUsers() {
        return service.getAllUsers();
    }

    @ApiOperation(value = "View a specific user retrieved by id", response = User.class)
    @GetMapping("/{id}")
    public User getUserById(@PathVariable int id) {
        try {
            return service.getUserById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


//    @GetMapping
//    public User getUserByUserName(@RequestParam(name = "username") String userName) {
//        try {
//            return service.getUserByUserName(userName);
//        } catch (EntityNotFoundException e) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
//        }
//    }

    @ApiOperation(value = "Create a new user", response = User.class)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public User createUser(@Valid @RequestBody UserDto userDto) {
        try {
            return service.createUser(userDto);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @ApiOperation(value = "Delete a user by userID", response = User.class)
    @DeleteMapping("/{id}")
    public User deleteUser(@PathVariable int id) {
        try {
            return service.deleteUser(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Update a specific user by userID", response = User.class)
    @PutMapping
    public User updateUser(@RequestBody User user) {
        try {
            return service.updateUser(user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @ApiOperation(value = "View user drinklist by userID", response = List.class)
    @GetMapping("/{id}/drinklist")
    public Set<Beer> getUserDrinkList(@PathVariable int id) {
        try {
            return service.getUserDrinkList(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Add beer to a user drinklist by userID")
    @PostMapping("/{id}/drinklist")
    public void addDrinkToDrinkList(@PathVariable int id,
                                    @RequestParam(name = "beerId") int beerId) {
        try {
            service.addBeerToDrinkList(id, beerId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @ApiOperation(value = "View user wishlist by userID", response = List.class)
    @GetMapping("/{id}/wishlist")
    public Set<Beer> getUserWishList(@PathVariable int id) {
        try {
            return service.getUserWishList(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Add beer to a user wishlist by userID")
    @PostMapping("/{id}/wishlist")
    public void addDrinkToWishList(@PathVariable int id,
                                   @RequestParam(name = "beerId") int beerId) {
        try {
            service.addBeerToWishList(id, beerId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}
