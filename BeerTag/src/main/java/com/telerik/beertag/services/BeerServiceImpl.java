package com.telerik.beertag.services;

import com.telerik.beertag.exceptions.*;
import com.telerik.beertag.helpers.BeerCollectionHelper;
import com.telerik.beertag.models.Beer;
import com.telerik.beertag.models.User;
import com.telerik.beertag.repositories.BeerRepository;
import com.telerik.beertag.repositories.RatingRepository;
import com.telerik.beertag.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Base64;
import java.util.Collections;
import java.util.List;

@Service
public class BeerServiceImpl implements BeerService {

    private static final String OPERATION_DENIED_TO_USER_EXCEPTION_MESSAGE = "User %s can not modify beer %s";
    private static final String BEER_ALREADY_EXISTS_EXCEPTION_MESSAGE = "Beer with name '%s' already exists!";

    private BeerRepository beerRepository;
    private UserRepository userRepository;
    private RatingRepository ratingRepository;

    @Autowired
    public BeerServiceImpl(@Qualifier("mariadb") BeerRepository beerRepository, UserRepository userRepository,
                           RatingRepository ratingRepository) {
        this.beerRepository = beerRepository;
        this.userRepository = userRepository;
        this.ratingRepository = ratingRepository;
    }

    @Override
    public List<Beer> getAllBeers() {
        return beerRepository.getAllBeers();
    }

    @Override
    public Page<Beer> getBeersPaged(Pageable pageable, String sortBy) {
        List<Beer> beers = getAllBeers();
        BeerCollectionHelper.sortBy(beers, sortBy);
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Beer> beerSubList;
        if (beers.size() < startItem) {
            beerSubList = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, beers.size());
            beerSubList = beers.subList(startItem, toIndex);
        }
        return new PageImpl<Beer>(beerSubList, PageRequest.of(currentPage, pageSize), beers.size());
    }

    @Override
    public Beer getBeerById(int id) {
        return beerRepository.getBeerById(id);
    }

    @Override
    public List<Beer> getBeerByName(String name) {
        return beerRepository.getBeerByName(name);
    }

    @Override
    public List<Beer> getBeerByStyle(int styleId) {
        return beerRepository.getBeerByStyle(styleId);
    }

    @Override
    public List<Beer> getBeerByCountry(int countryId) {
        return beerRepository.getBeerByCountry(countryId);
    }

    @Override
    public List<Beer> filterBeerByTag(String tag) {
        return beerRepository.filterBeerByTag(tag);
    }

    @Override
    public List<Beer> sortBeerBy(String condition) {
        return beerRepository.sortBeerBy(condition);
    }

    @Override
    public Beer createBeer(Beer beer) {
        if (beerRepository.checkIfBeerExists(beer.getName())) {
            throw new DuplicateEntityException(
                    String.format(BEER_ALREADY_EXISTS_EXCEPTION_MESSAGE, beer.getName()));
        }
        beerRepository.createBeer(beer);
        return beer;
    }

    @Override
    public Beer updateBeer(int id, Beer beer) {
        if (!beerRepository.checkIfBeerExists(beerRepository.getBeerById(id).getName())) {
            throw new EntityNotFoundException("Beer", id);
        }

//        if (beer.getUser().getUserId() != user.getUserId()
//                && user.getRoles().stream().noneMatch(role -> role.getRole().equals("admin"))) {
//            throw new InvalidOperationException(
//                    String.format(OPERATION_DENIED_TO_USER_EXCEPTION_MESSAGE, user.getUsername(), id));
//        }

        return beerRepository.updateBeer(id, beer);
    }

    @Override
    public void deleteBeer(int id) {
        if (!beerRepository.checkIfBeerExists(beerRepository.getBeerById(id).getName())) {
            throw new EntityNotFoundException("Beer", id);
        }
        beerRepository.deleteBeer(id);
    }

    @Override
    public void rateBeer(String beerName, int userId, double rating)
            throws CantRateBeerException, RatingOutOfRangeException {
        List<Beer> beers = beerRepository.getBeerByName(beerName);
        if (beers.isEmpty()) {
            throw new EntityNotFoundException("Beer", beerName);
        }
        Beer beer = beers.get(0);
        User user = userRepository.getUserById(userId);
        if (user == null) {
            throw new EntityNotFoundException("User", userId);
        }
        if (rating < 1 || rating > 5) throw new RatingOutOfRangeException("Beer rating must be between 1 and 5!");
        if (user.getDrinkList().stream().anyMatch(beer1 -> beer1.getName().equals(beer.getName()))) {
            ratingRepository.rate(user, beer, rating);
        } else throw new CantRateBeerException("You can rate only beers that you have tasted");
        ratingRepository.getRating(beer);
    }

    @Override
    public void addPictureToBeer(MultipartFile picture, int beerId) {
        Beer beer = getBeerById(beerId);
        try {
            byte[] pictureBytes = picture.getBytes();
            String encodedPicture = Base64.getEncoder().encodeToString(pictureBytes);
            beer.setPicture(encodedPicture);
            beerRepository.updateBeer(beerId, beer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removeBeerPicture(int beerId) {
        Beer beer = getBeerById(beerId);
        beer.setPicture(null);
        beerRepository.updateBeer(beerId, beer);
    }

    @Override
    public List<Beer> getBeersByUser(int userID) {
        return beerRepository.getBeersByUser(userID);
    }
}