package com.telerik.beertag.exceptions;

public class RatingOutOfRangeException extends Exception {
    public RatingOutOfRangeException(String message){
        super(message);
    }
}
