package com.telerik.beertag.services;

import com.telerik.beertag.exceptions.EntityNotFoundException;
import com.telerik.beertag.models.Tag;
import com.telerik.beertag.repositories.TagRepository;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;

import static com.telerik.beertag.Factory.createTag;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTests {

    @Mock
    TagRepository mockRepository;

    @InjectMocks
    TagServiceImpl service;

    @Test
    public void addTag_Should_AddTag() {
        //Arrange
        Tag tag = createTag();
        Mockito.when(mockRepository.addTag(tag))
                .thenReturn(tag);

        //Act

        service.addTag(tag);

        //Assert

        Mockito.verify(mockRepository, Mockito.times(1)).addTag(any());

    }

    @Test
    public void removeTag_Should_removeTag_When_Tag_Exists() throws EntityNotFoundException {
        //Arrange
        Tag tag = createTag();
        Mockito.when(mockRepository.checkIfTagExists(anyInt()))
                .thenReturn(true);
        //Act

        service.removeTag(0);

        //Assert

        Mockito.verify(mockRepository, Mockito.times(1)).removeTag(0);

    }

    @Test
    public void removeTag_Should_Throw_When_Tag_DoesNotExists() throws EntityNotFoundException {
        //Arrange
        Tag tag = createTag();
        Mockito.when(mockRepository.checkIfTagExists(anyInt()))
                .thenReturn(false);

        //Act
        // Assert

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.removeTag(0));

    }

    @Test
    public void updateTag_Should_updateTag_When_Tag_Exists() throws EntityNotFoundException {
        //Arrange
        Tag tag = createTag();
        Mockito.when(mockRepository.checkIfTagExists(anyInt()))
                .thenReturn(true);
        //Act

        service.updateTag(0, tag);

        //Assert

        Mockito.verify(mockRepository, Mockito.times(1)).updateTag(0, tag);

    }

    @Test
    public void updateTag_Should_Throw_When_Tag_DoesNotExists() throws EntityNotFoundException {
        //Arrange
        Tag tag = createTag();
        Mockito.when(mockRepository.checkIfTagExists(anyInt()))
                .thenReturn(false);

        //Act
        // Assert

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.updateTag(0, tag));

    }

    @Test
    public void getAllTags_Should_return_AllTags() {
        //Arrange
        Tag tag = createTag();
        Mockito.when(mockRepository.getAllTags())
                .thenReturn(new HashSet<>());
        //Act

        service.getAllTags();

        //Assert

        Mockito.verify(mockRepository, Mockito.times(1)).getAllTags();

    }

    @Test
    public void getTagById_Should_returnTag_When_Tag_Exists() throws EntityNotFoundException {
        //Arrange
        Mockito.when(mockRepository.checkIfTagExists(anyInt()))
                .thenReturn(true);
        //Act

        service.getTagById(0);

        //Assert

        Mockito.verify(mockRepository, Mockito.times(1)).getTagById(0);

    }

    @Test
    public void getTagById_Should_Throw_When_Tag_DoesNotExists() throws EntityNotFoundException {
        //Arrange
        Mockito.when(mockRepository.checkIfTagExists(anyInt()))
                .thenReturn(false);

        //Act
        // Assert

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.getTagById(0));

    }

    @Test
    public void getTagByName_Should_Throw_When_Tag_DoesNotExists() throws EntityNotFoundException {
        //Arrange
        Tag tag = createTag();
        Mockito.when(mockRepository.getAllTags())
                .thenReturn(new HashSet<>());

        //Act
        // Assert

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.getTagByName(tag.getName()));

    }

    @Test
    public void getTagByName_Should_returnTag_When_Tag_Exists() throws EntityNotFoundException {
        //Arrange
        Tag tag = createTag();

        Mockito.when(mockRepository.getAllTags())
                .thenReturn(new HashSet<Tag>(){{
                    add(tag);
                }});
        //Act

        service.getTagByName(tag.getName());

        //Assert

        Mockito.verify(mockRepository, Mockito.times(1)).getTagByName(tag.getName());

    }

    @Test
    public void checkIfTagExists_Should_returnTrue_When_Tag_Exists() throws EntityNotFoundException {
        //Arrange
        Tag tag = createTag();

        //Act

        service.checkIfTagExists(0);

        //Assert

        Mockito.verify(mockRepository, Mockito.times(1)).checkIfTagExists(0);

    }


}
