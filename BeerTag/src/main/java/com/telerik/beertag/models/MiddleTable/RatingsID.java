package com.telerik.beertag.models.MiddleTable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class RatingsID implements Serializable {

    @Column(name = "user_id")
    private int userId;

    @Column(name = "beer_id")
    private int beerId;

    public RatingsID() {
    }

    public RatingsID(Integer userId, Integer beerId) {
        this.beerId = beerId;
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getBeerId() {
        return beerId;
    }

    public void setBeerId(int beerId) {
        this.beerId = beerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RatingsID)) return false;
        RatingsID ratingsID = (RatingsID) o;
        return getUserId() == ratingsID.getUserId() &&
                getBeerId() == ratingsID.getBeerId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUserId(), getBeerId());
    }
}
