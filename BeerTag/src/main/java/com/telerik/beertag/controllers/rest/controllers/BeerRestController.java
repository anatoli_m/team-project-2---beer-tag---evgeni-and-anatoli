package com.telerik.beertag.controllers.rest.controllers;

import com.telerik.beertag.exceptions.*;
import com.telerik.beertag.helpers.BeerCollectionHelper;
import com.telerik.beertag.models.Beer;
import com.telerik.beertag.models.BeerDto;
import com.telerik.beertag.models.DtoMapper;
import com.telerik.beertag.models.User;
import com.telerik.beertag.repositories.StyleRepository;
import com.telerik.beertag.services.BeerServiceImpl;
import com.telerik.beertag.services.UserServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Api(description = "Operations pertaining to beer items in BeerTag")
@RestController
@RequestMapping("/api/beers")
public class BeerRestController {

    private BeerServiceImpl beerService;
    private UserServiceImpl userService;
    private DtoMapper mapper;

    @Autowired
    public BeerRestController(BeerServiceImpl beerService, UserServiceImpl userService, DtoMapper mapper) {
        this.beerService = beerService;
        this.userService = userService;
        this.mapper = mapper;
    }

    @ApiOperation(value = "View a list of all beers", response = List.class)
    @GetMapping
    public List<Beer> getAllBeers(@RequestParam(required = false, defaultValue = "") String name,
                                  @RequestParam(required = false, defaultValue = "") String style,
                                  @RequestParam(required = false, defaultValue = "") String countryId,
                                  @RequestParam(required = false, defaultValue = "") String sortBy,
                                  @RequestParam(required = false, defaultValue = "") String filterByTag) {
        if (!name.isEmpty()) {
            return beerService.getBeerByName(name);
        } else if (!style.isEmpty()) {
            return beerService.getBeerByStyle(Integer.parseInt(style));
        } else if (!countryId.isEmpty()) {
            return beerService.getBeerByCountry(Integer.parseInt(countryId));
        } else if (!sortBy.isEmpty()) {
            return beerService.sortBeerBy(sortBy);
        } else if (!filterByTag.isEmpty()) {
            return beerService.filterBeerByTag(filterByTag);
        }
        return beerService.getAllBeers();
    }

    @ApiOperation(value = "View a specific beer by beerID", response = Beer.class)
    @GetMapping("/{id}")
    public Beer getBeerById(@PathVariable int id) {
        try {
            return beerService.getBeerById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "View a list of beers created by specific user specified by userID",
            response = List.class)
    @GetMapping("/user")
    public List<Beer> getBeerByUser(@RequestParam(name = "id") int userID) {
        try {
            return beerService.getBeersByUser(userID);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Delete beer specified by beerID", response = Beer.class)
    @DeleteMapping("/{id}")
    public Beer deleteBeer(@PathVariable int id) {
        try {
            Beer beerToDelete = beerService.getBeerById(id);
            beerService.deleteBeer(beerToDelete.getId());
            return beerToDelete;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Create a new beer", response = Beer.class)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void createBeer(@RequestBody @Valid BeerDto beerDto,
                           @RequestHeader(name = "Authorization") String userName) {
        try {
            User requestUser = userService.getUserByUserName(userName);
            Beer newBeer = mapper.fromDto(beerDto, requestUser);
            beerService.createBeer(newBeer);
        } catch (EntityNotFoundException nfe) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, nfe.getMessage());
        } catch (DuplicateEntityException dee) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dee.getMessage());
        }
    }

    @ApiOperation(value = "Update a beer", response = Beer.class)
    @PutMapping("/{id}")
    public Beer updateBeer(@PathVariable int id,
                           @RequestBody @Valid BeerDto beerDto,
                           @RequestHeader(name = "Authorization") String userName) {
        try {
            User requestUser = userService.getUserByUserName(userName);
            Beer beerToUpdate = mapper.fromDto(beerDto);
            return beerService.updateBeer(id, beerToUpdate);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new InvalidOperationException(e.getMessage());
        }
    }

    @ApiOperation(value = "Rate a beer")
    @PutMapping("/{beerName}/rate/{userId}/{rating}")
    public void rateBeer(@PathVariable String beerName, @PathVariable int userId, @PathVariable double rating) {
        try {
            beerService.rateBeer(beerName, userId, rating);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (CantRateBeerException ex) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ex.getMessage());
        } catch (RatingOutOfRangeException r) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, r.getMessage());
        }
    }
}
