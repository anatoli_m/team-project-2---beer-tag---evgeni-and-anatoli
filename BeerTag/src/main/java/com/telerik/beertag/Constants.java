package com.telerik.beertag;

public class Constants {

    // BEER VALIDATION
    public static final int BEER_MIN_NAME_LENGTH = 2;
    public static final int BEER_MAX_NAME_LENGTH = 25;
    public static final int BEER_MIN_DESCRIPTION_LENGTH = 10;
    public static final int BEER_MAX_DESCRIPTION_LENGTH = 150;
    public static final String BEER_ID_ERROR_MESSAGE = "Beer id should be positive or zero.";
    public static final String BEER_STYLE_ID_ERROR_MESSAGE = "Beer style id should be between 1 and 5";
    public static final String BEER_ABV_ERROR_MESSAGE = "Beer abv should be positive or zero.";

}
