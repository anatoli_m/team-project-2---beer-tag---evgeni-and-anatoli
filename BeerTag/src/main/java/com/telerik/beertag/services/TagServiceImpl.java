package com.telerik.beertag.services;

import com.telerik.beertag.exceptions.EntityNotFoundException;
import com.telerik.beertag.models.Tag;
import com.telerik.beertag.repositories.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class TagServiceImpl implements TagService {

    private TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public Tag addTag(Tag newTag) {
       return tagRepository.addTag(newTag);
    }

    @Override
    public void removeTag(int id) {
        if (!tagRepository.checkIfTagExists(id)) {
            throw new EntityNotFoundException("Tag", id);
        }
        tagRepository.removeTag(id);
    }

    @Override
    public Tag updateTag(int id, Tag tagToUpdate) {
        if (!tagRepository.checkIfTagExists(id)) {
            throw new EntityNotFoundException("Tag", id);
        }
        return tagRepository.updateTag(id, tagToUpdate);
    }

    @Override
    public Set<Tag> getAllTags() {
        return tagRepository.getAllTags();
    }

    @Override
    public Tag getTagById(int id) {
        if (!tagRepository.checkIfTagExists(id)) {
            throw new EntityNotFoundException("Tag", id);
        }
        return tagRepository.getTagById(id);
    }

    @Override
    public Tag getTagByName(String tag) {
        if (tagRepository.getAllTags().stream().noneMatch(tag1 -> tag1.getName().equals(tag))) {
            throw new EntityNotFoundException("Tag", tag);
        }
        return tagRepository.getTagByName(tag);
    }

    @Override
    public boolean checkIfTagExists(int id) {
        return tagRepository.checkIfTagExists(id);
    }
}
