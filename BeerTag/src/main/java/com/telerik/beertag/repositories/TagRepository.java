package com.telerik.beertag.repositories;

import com.telerik.beertag.models.Style;
import com.telerik.beertag.models.Tag;

import java.util.List;
import java.util.Set;

public interface TagRepository {

    Tag addTag(Tag newTag);

    void removeTag(int id);

    Tag updateTag(int id, Tag newTag);

    Set<Tag> getAllTags();

    Tag getTagById(int id);

    Tag getTagByName(String tag);

   boolean checkIfTagExists(int id);
}
