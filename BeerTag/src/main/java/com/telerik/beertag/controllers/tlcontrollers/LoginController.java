package com.telerik.beertag.controllers.tlcontrollers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String showLogin() {
        return "user-login";
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String showAdmin() {
        return "admin";
    }

    @RequestMapping(value = "/access-denied", method = RequestMethod.GET)
    public String showAccessDenied() {
        return "access-denied";
    }
}
