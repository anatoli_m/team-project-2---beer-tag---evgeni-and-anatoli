DROP DATABASE IF EXISTS beertagdb;

CREATE DATABASE beertagdb;

USE beertagdb;

CREATE TABLE countries (
country_id INT AUTO_INCREMENT PRIMARY KEY,
country_code VARCHAR(2) NOT NULL,
country_name VARCHAR(100) NOT NULL
);

CREATE TABLE breweries (
brewery_id INT AUTO_INCREMENT PRIMARY KEY,
brewery_name VARCHAR(150) NOT NULL,
country_id INT NOT NULL,

FOREIGN KEY (country_id) REFERENCES countries(country_id)
);

CREATE TABLE styles (
style_id INT AUTO_INCREMENT PRIMARY KEY,
style_name VARCHAR(50) NOT NULL
);

CREATE TABLE users (
user_id INT AUTO_INCREMENT PRIMARY KEY,
user_name VARCHAR(20) NOT NULL UNIQUE,
password VARCHAR(200) NOT NULL, 
first_name VARCHAR(50) NOT NULL,
last_name VARCHAR(50) NOT NULL,
email VARCHAR(50) NOT NULL UNIQUE,
avatar MEDIUMTEXT,
is_deleted BOOLEAN NOT NULL DEFAULT 0
);

CREATE TABLE  roles (
role_id INT AUTO_INCREMENT PRIMARY KEY,
role VARCHAR(10) UNIQUE
);

CREATE TABLE users_roles (
users_roles_id INT AUTO_INCREMENT PRIMARY KEY,
user_id INT NOT NULL,
role_id INT NOT NULL DEFAULT 2,

FOREIGN KEY (user_id) REFERENCES users(user_id),
FOREIGN KEY (role_id) REFERENCES roles(role_id)
);

CREATE TABLE beers (
beer_id INT AUTO_INCREMENT PRIMARY KEY,
beer_name VARCHAR(50) NOT NULL,
description TEXT NOT NULL,
brewery_id INT NOT NULL,
style_id INT NOT NULL,
beer_ABV DOUBLE NOT NULL,
picture MEDIUMTEXT,
user_id INT NOT NULL,
is_deleted BOOLEAN NOT NULL DEFAULT 0,
rating DOUBLE NOT NULL DEFAULT 0.00,

FOREIGN KEY (brewery_id) REFERENCES breweries(brewery_id),
FOREIGN KEY (style_id) REFERENCES styles(style_id),
FOREIGN KEY (user_id) REFERENCES users(user_id)
);

CREATE TABLE tags (
tag_id INT AUTO_INCREMENT PRIMARY KEY,
tag_name VARCHAR(50)
);

CREATE TABLE beer_tag (
beer_tag_id INT AUTO_INCREMENT PRIMARY KEY,
beer_id INT NOT NULL,
tag_id INT NOT NULL,

FOREIGN KEY (beer_id) REFERENCES beers(beer_id),
FOREIGN KEY (tag_id) REFERENCES tags(tag_id)
);

CREATE TABLE user_drinks (
user_drink_id INT AUTO_INCREMENT PRIMARY KEY,
beer_id INT NOT NULL,
user_id INT NOT NULL,
rating DOUBLE NOT NULL DEFAULT 0,

FOREIGN KEY (beer_id) REFERENCES beers(beer_id),
FOREIGN KEY (user_id) REFERENCES users(user_id)
);

CREATE TABLE user_beer_wishes (
user_beer_wish_id INT AUTO_INCREMENT PRIMARY KEY,
beer_id INT NOT NULL,
user_id INT NOT NULL,

FOREIGN KEY (beer_id) REFERENCES beers(beer_id),
FOREIGN KEY (user_id) REFERENCES users(user_id)
);

# CREATE TABLE ratings (
# rating_id INT AUTO_INCREMENT PRIMARY KEY,
# beer_id INT NOT NULL,
# user_id INT NOT NULL,
# rating INT NOT NULL,
#
# FOREIGN KEY (beer_id) REFERENCES beers(beer_id),
# FOREIGN KEY (user_id) REFERENCES users(user_id)
# );