package com.telerik.beertag;

import com.telerik.beertag.models.*;

import java.util.*;

public class Factory {

    public static Beer createBeer() {
        List<String> tags1 = Arrays.asList("nice", "bitter");
        Beer beer = new Beer("Zagorka", "Test description 1",
                4.5, "Photos");
        beer.setId(0);
        return beer;
    }

    public static Style createStyle() {
        Style style = new Style("testStyle");
        style.setId(0);
        return style;
    }

    public static UserDto createUserDto() {
        UserDto userDto = new UserDto();
        userDto.setFirstName("firstName");
        userDto.setLastName("lastName");
        userDto.setUsername("username");
        userDto.setEmail("test@email.com");
        userDto.setPassword("pass");
        userDto.setMatchingPassword("pass");
        return userDto;
    }

    public static User createUser() {
        User user = new User();
        user.setFirstName("firstName");
        user.setLastName("lastName");
        user.setUsername("username");
        user.setEmail("test@email.com");
        user.setPassword("pass");
        return user;
    }

    public static User createUserNoRights() {
        User user = new User(2, "Maurice", "Laas", "maurice@gmail.com");
        Role role = new Role();
        role.setRole("none");
        user.setRoles(new HashSet<>(Collections.singletonList(role)));
        return user;
    }

    public static Tag createTag() {
        Tag tag = new Tag("testTag");
        tag.setTagId(0);
        return tag;
    }

    public static Country createCountry() {
        Country country = new Country("testCountry");
        country.setId(0);
        return country;
    }

    public static Brewery createBrewery() {
        Brewery brewery = new Brewery("testBrewery");
        brewery.setId(0);
        return brewery;
    }
}
