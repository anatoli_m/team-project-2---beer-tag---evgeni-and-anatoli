package com.telerik.beertag.exceptions;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(String itemType, int id) {
        this(itemType, "id", String.valueOf(id));
    }

    public EntityNotFoundException(String itemType, String userName) {
        this(itemType, "username", userName);
    }

    private EntityNotFoundException(String itemType, String attribute, String value) {
        super(String.format("Can not find %s with %s %s", itemType, attribute, value));
    }
}
