# Team Project 2 - Beer Tag - Evgeni and Anatoli 

Spring Boot REST API
This is a BEER TAG web application. BEER TAG enables your users to manage all the beers
that they have drank and want to drink. Each beer has detailed information about it
from the ABV (alcohol by volume) to the style and description. Data is community driven
and every beer lover can add new beers and edit missing information on already existing ones.
Also, BEER TAG allows you to rate a beer and calculates average rating from different users.

This is our API Documentation:
http://localhost:8080/swagger-ui.html

This is our trello board:
https://trello.com/b/AGUWPOje/team-project-2-beer-tag



Created by Anatoli Manolov, Evgeni Minkov