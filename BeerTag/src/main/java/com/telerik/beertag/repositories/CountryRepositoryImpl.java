package com.telerik.beertag.repositories;

import com.telerik.beertag.models.Country;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CountryRepositoryImpl implements CountryRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public CountryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Country createCountry(Country newCountry) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(newCountry);
            session.getTransaction().commit();
        }
        return newCountry;
    }

    @Override
    public Country deleteCountry(int id) {
        try (Session session = sessionFactory.openSession()){
            Country country = getCountryById(id);
            session.beginTransaction();
            session.delete(country);
            session.getTransaction().commit();
            return country;
        }
    }

    @Override
    public List<Country> getCountries() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("FROM Country", Country.class)
                    .list();
        }
    }

    @Override
    public Country getCountryById(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Country.class, id);
        }
    }

    @Override
    public Country updateCountry(Country country) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(country);
            session.getTransaction().commit();
        }
        return country;
    }

    @Override
    public boolean checkIfCountryExists(String country) {
        try (Session session = sessionFactory.openSession()) {
            String hql = String.format("FROM Country WHERE name = '%s'", country);
            return !session.createQuery(hql).list().isEmpty();
        }
    }

    @Override
    public boolean checkIfCountryExists(int id) {
        try (Session session = sessionFactory.openSession()) {
            Country country = session.get(Country.class, id);
            return country != null;
        }
    }

}
