package com.telerik.beertag.repositories;

import com.telerik.beertag.models.Beer;
import com.telerik.beertag.models.MiddleTable.Ratings;
import com.telerik.beertag.models.MiddleTable.RatingsID;
import com.telerik.beertag.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Qualifier("mariadb")
public class RatingRepositoryImpl implements RatingRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public RatingRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void rate(User user, Beer beer, double rating) {
        try(Session session = sessionFactory.openSession()) {
            RatingsID id = new RatingsID();
            id.setUserId(user.getUserId());
            id.setBeerId(beer.getId());
            Ratings newRating = new Ratings();
            newRating.setId(id);
            newRating.setRating(rating);
            session.beginTransaction();
            session.update(newRating);
            session.getTransaction().commit();
        }
    }

    @Override
    public double getRating(Beer beer) {
        try(Session session = sessionFactory.openSession()) {
            Query<Ratings> query = session.createQuery("from Ratings where beer_id= :id and rating is not null", Ratings.class);
            query.setParameter("id", beer.getId());
            double rating = (query.list().stream().mapToDouble(Ratings::getRating).sum()) / query.list().size();
            session.beginTransaction();
            beer.setRating(rating);
            session.update(beer);
            session.getTransaction().commit();

            return rating;
        }
    }
}
