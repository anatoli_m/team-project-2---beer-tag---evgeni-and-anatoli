package com.telerik.beertag.controllers.tlcontrollers;

import com.telerik.beertag.exceptions.DuplicateEntityException;
import com.telerik.beertag.exceptions.WrongPasswordException;
import com.telerik.beertag.models.User;
import com.telerik.beertag.services.BeerService;
import com.telerik.beertag.services.UserService;
import com.telerik.beertag.validation.ValidPassword;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.security.Principal;

@Controller
@RequestMapping("/user")
public class UsersController {

    private UserService userService;
    private BeerService beerService;

    @Autowired
    public UsersController(UserService userService, BeerService beerService) {
        this.userService = userService;
        this.beerService = beerService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String showUserProfile(Model model,
                                  Principal principal) {
        model.addAttribute("user", userService.getUserByUserName(principal.getName()));
        model.addAttribute("users", userService.getAllUsers());
        model.addAttribute("beers", beerService.getBeersByUser(userService.getUserByUserName(principal.getName()).getUserId()));

        return "user";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String changeProfilePicture(@RequestParam("profilePicture") MultipartFile profilePicture,
                                       Principal principal,
                                       Model model) {
        userService.addProfilePicture(principal.getName(), profilePicture);
        model.addAttribute("user", userService.getUserByUserName(principal.getName()));
        return "user";
    }

    @RequestMapping("/addToDrinklist")
    public String addBeerToDrinkList(@RequestParam(name = "beerID") int beerID,
                                     Principal principal) {
        User user = userService.getUserByUserName(principal.getName());
        userService.addBeerToDrinkList(user.getUserId(), beerID);
        return "redirect:/user";
    }

    @RequestMapping("/removeFromDrinklist")
    public String removeBeerFromDrinkList(@RequestParam(name = "beerID") int beerID,
                                          Principal principal) {
        User user = userService.getUserByUserName(principal.getName());
        userService.removeBeerFromDrinkList(user.getUserId(), beerID);
        return "redirect:/user";
    }

    @RequestMapping("/addToWishlist")
    public String addBeerToWishList(@RequestParam(name = "beerID") int beerID,
                                    Principal principal) {
        User user = userService.getUserByUserName(principal.getName());
        userService.addBeerToWishList(user.getUserId(), beerID);
        return "redirect:/user";
    }

    @RequestMapping("/removeFromWishlist")
    public String removeBeerFromWishList(@RequestParam(name = "beerID") int beerID,
                                         Principal principal) {
        User user = userService.getUserByUserName(principal.getName());
        userService.removeBeerFromWishList(user.getUserId(), beerID);
        return "redirect:/user";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable int id) {
        userService.deleteUser(id);
        return "redirect:/user";
    }

    @RequestMapping(value = "/updatePassword", method = RequestMethod.POST)
    public String changeUserPassword(@RequestParam("oldPassword") String oldPassword,
                                     @ValidPassword @RequestParam("newPassword") String newPassword,
                                     Principal principal,
                                     Model model) {
        User user = userService.getUserByUserName(principal.getName());
        model.addAttribute("user", user);
        try {
            userService.changeUserPassword(user.getUsername(), oldPassword, newPassword);
        } catch (WrongPasswordException e) {
            model.addAttribute("passError", "Wrong password");
            return "change-user-details";
        }
        model.addAttribute("passChangeSuccess", "Profile updated successfully!");
        return "change-user-details";
    }

    @GetMapping(value = "/edit")
    public String editUserDetails(Model model,
                                  Principal principal) {
        User user = userService.getUserByUserName(principal.getName());
        model.addAttribute("user", user);
        return "change-user-details";
    }

    @RequestMapping(value = "/updateProfile", method = RequestMethod.POST)
    public String updateUserProfile(@RequestParam("firstName") String firstName,
                                    @RequestParam("lastName") String lastName,
                                    @RequestParam("email") String email,
                                    Principal principal,
                                    Model model) {
        User user = userService.getUserByUserName(principal.getName());
        try {
            userService.updateUserDetails(user, firstName, lastName, email);
            model.addAttribute("success", "Profile updated successfully!");
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", "Email already exists");
        }
        model.addAttribute("user", user);
        return "change-user-details";
    }

    @GetMapping(value = "/deleteProfile")
    public String deleteUserProfile(Principal principal) {
        User user = userService.getUserByUserName(principal.getName());
        userService.deleteUser(user.getUserId());
        return "redirect:/login?deleted";
    }
}
