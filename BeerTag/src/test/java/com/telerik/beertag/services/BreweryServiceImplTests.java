package com.telerik.beertag.services;

import com.telerik.beertag.exceptions.DuplicateEntityException;
import com.telerik.beertag.exceptions.EntityNotFoundException;

import com.telerik.beertag.models.Brewery;
import com.telerik.beertag.repositories.BreweryRepositoryImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.telerik.beertag.Factory.createBrewery;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class BreweryServiceImplTests {

    @Mock
    BreweryRepositoryImpl mockRepository;

    @InjectMocks
    BreweryServiceImpl mockService;

    @Test
    public void createBreweryShould_CallRepository() {
        //Arrange
        Brewery brewery = createBrewery();

        //Act
        mockService.createBrewery(brewery);

        //Assert
        Mockito.verify(mockRepository, times(1)).createBrewery(brewery);
    }

    @Test
    public void createBreweryShouldThrow_WhenBreweryAlreadyExists() {
        //Arrange
        Mockito.when(mockRepository.checkIfBreweryExists(anyString()))
                .thenReturn(true);

        //Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.createBrewery(createBrewery()));
    }

    @Test
    public void createBreweryShould_ReturnNewBrewery() {
        //Arrange
        Brewery expectedBrewery = createBrewery();
        Mockito.when(mockRepository.createBrewery(expectedBrewery)).thenReturn(expectedBrewery);

        //Act
        Brewery newBrewery = mockService.createBrewery(expectedBrewery);

        //Assert
        Assert.assertSame(expectedBrewery, newBrewery);
    }

    @Test
    public void deleteBreweryShould_CallRepository() {
        //Arrange
        Mockito.when(mockRepository.checkIfBreweryExists(anyInt())).thenReturn(true);

        //Act
        mockService.deleteBrewery(anyInt());

        //Assert
        Mockito.verify(mockRepository, times(1)).deleteBrewery(anyInt());
    }

    @Test
    public void deleteBreweryShouldThrow_WhenBreweryDoesNotExists() {
        //Arrange
        Mockito.when(mockRepository.checkIfBreweryExists(anyInt())).thenReturn(false);

        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockService.deleteBrewery(anyInt()));
    }

    @Test
    public void deleteBreweryShould_ReturnDeletedBrewery() {
        //Arrange
        Brewery expectedBrewery = createBrewery();
        Mockito.when(mockRepository.checkIfBreweryExists(anyInt())).thenReturn(true);
        Mockito.when(mockRepository.deleteBrewery(anyInt())).thenReturn(expectedBrewery);

        //Act
        Brewery deletedBrewery = mockService.deleteBrewery(expectedBrewery.getId());

        //Assert
        Assert.assertSame(expectedBrewery, deletedBrewery);
    }

    @Test
    public void getBreweryByIdShould_CallRepository() {
        //Arrange
        Mockito.when(mockRepository.checkIfBreweryExists(anyInt())).thenReturn(true);

        //Act
        mockService.getBreweryById(anyInt());

        //Assert
        Mockito.verify(mockRepository, times(1)).getBreweryById(anyInt());
    }

    @Test
    public void getBreweryByIdShouldThrow_WhenBreweryDoesNotExists() {
        //Arrange
        Mockito.when(mockRepository.checkIfBreweryExists(anyInt())).thenReturn(false);

        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockService.getBreweryById(anyInt()));
    }

    @Test
    public void getBreweryByIdShould_ReturnBrewery() {
        //Arrange
        Brewery expectedBrewery = createBrewery();
        Mockito.when(mockRepository.checkIfBreweryExists(anyInt())).thenReturn(true);
        Mockito.when(mockRepository.getBreweryById(anyInt())).thenReturn(expectedBrewery);

        //Act
        Brewery returnedBrewery = mockService.getBreweryById(anyInt());

        //Assert
        Assert.assertSame(expectedBrewery, returnedBrewery);
    }

    @Test
    public void updateBreweryShould_CallRepository() {
        //Arrange
        Mockito.when(mockRepository.checkIfBreweryExists(anyInt())).thenReturn(true);

        //Act
        mockService.updateBrewery(createBrewery());

        //Assert
        Mockito.verify(mockRepository, times(1))
                .updateBrewery(any(Brewery.class));
    }

    @Test
    public void updateBreweryShouldThrow_WhenBreweryDoesNotExists() {
        //Arrange
        Mockito.when(mockRepository.checkIfBreweryExists(anyInt())).thenReturn(false);

        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockService.updateBrewery(createBrewery()));
    }

    @Test
    public void updateBreweryShould_ReturnUpdatedBrewery() {
        //Arrange
        Brewery expectedBrewery = createBrewery();
        Mockito.when(mockRepository.checkIfBreweryExists(anyInt())).thenReturn(true);
        Mockito.when(mockRepository.updateBrewery(any(Brewery.class)))
                .thenReturn(expectedBrewery);

        //Act
        Brewery updatedBrewery = mockService.updateBrewery(expectedBrewery);

        //Assert
        Assert.assertSame(expectedBrewery, updatedBrewery);
    }
}