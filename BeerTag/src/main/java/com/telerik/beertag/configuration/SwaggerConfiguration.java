package com.telerik.beertag.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.regex("/api.*"))
                .build()
                .apiInfo(apiEndPointsInfo());
    }

    private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder().title("Spring Boot REST API")
                .description("This is a BEER TAG web application. BEER TAG enables your users to manage all the " +
                        "beers that they have drank and want to drink. Each beer has detailed information about " +
                        "it from the ABV (alcohol by volume) to the style and description. Data is community driven " +
                        "and every beer lover can add new beers and edit missing information on already existing " +
                        "ones. Also, BEER TAG allows you to rate a beer and calculates average rating from " +
                        "different users.")
                .contact(new Contact("Anatoli Manolov, Evgeni Minkov",
                        "localhost:8080/about",
                        "manolov.anatoli@gmail.com"))
                .license("Apache 2.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                .version("1.0.0")
                .build();
    }
}