package com.telerik.beertag.repositories;

import com.telerik.beertag.models.Brewery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BreweryRepositoryImpl implements BreweryRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public BreweryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Brewery createBrewery(Brewery newBrewery) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(newBrewery);
            session.getTransaction().commit();
        }
        return newBrewery;
    }

    @Override
    public Brewery deleteBrewery(int id) {
        try (Session session = sessionFactory.openSession()) {
            Brewery breweryToDelete = getBreweryById(id);
            session.beginTransaction();
            session.delete(breweryToDelete);
            session.getTransaction().commit();
            return breweryToDelete;
        }
    }

    @Override
    public List<Brewery> getBreweries() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("FROM Brewery", Brewery.class)
                    .list();
        }
    }

    @Override
    public Brewery getBreweryById(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Brewery.class, id);
        }
    }

    @Override
    public Brewery updateBrewery(Brewery brewery) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(brewery);
            session.getTransaction().commit();
            return brewery;
        }
    }

    @Override
    public boolean checkIfBreweryExists(String brewery) {
        try (Session session = sessionFactory.openSession()) {
            String hql = String.format("FROM Brewery WHERE name = '%s'", brewery);
            return !session.createQuery(hql).list().isEmpty();
        }
    }

    @Override
    public boolean checkIfBreweryExists(int id) {
        try (Session session = sessionFactory.openSession()) {
            Brewery brewery = session.get(Brewery.class, id);
            return brewery != null;
        }
    }
}
