package com.telerik.beertag.controllers.tlcontrollers;

import com.telerik.beertag.exceptions.CantRateBeerException;
import com.telerik.beertag.exceptions.DuplicateEntityException;
import com.telerik.beertag.exceptions.RatingOutOfRangeException;
import com.telerik.beertag.models.*;
import com.telerik.beertag.models.MiddleTable.Ratings;
import com.telerik.beertag.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class BeersController {

    private BeerServiceImpl beerService;
    private StyleServiceImpl styleService;
    private BreweryServiceImpl breweryService;
    private CountryServiceImpl countryService;
    private TagServiceImpl tagService;
    private DtoMapper mapper;
    private UserService userService;

    @Autowired
    public BeersController(BeerServiceImpl beerService,
                           DtoMapper mapper,
                           StyleServiceImpl styleService,
                           BreweryServiceImpl breweryService,
                           CountryServiceImpl countryService,
                           TagServiceImpl tagService,
                           UserService userService) {
        this.beerService = beerService;
        this.mapper = mapper;
        this.styleService = styleService;
        this.breweryService = breweryService;
        this.countryService = countryService;
        this.tagService = tagService;
        this.userService = userService;
    }

    @GetMapping("/beers")
    public String showBeers(@RequestParam(name = "page", defaultValue = "1") int currentPage,
                            @RequestParam(name = "size", defaultValue = "12") int pageSize,
                            @RequestParam(required = false, defaultValue = "name") String sortBy,
                            Model model,
                            Principal principal) {
        Page<Beer> beerPage = beerService.getBeersPaged(PageRequest.of(currentPage - 1, pageSize), sortBy);
        model.addAttribute("beerPage", beerPage);
        model.addAttribute("sortBy", sortBy);
        int totalPages = beerPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        if (principal != null) {
            User user = userService.getUserByUserName(principal.getName());
            model.addAttribute("user", user);
        }
        return "allbeers";
    }

    @GetMapping("/beers/new")
    public String showNewBeerForm(Model model) {
        List<Style> styles = styleService.getAllStyles();
        List<Brewery> breweries = breweryService.getBreweries();
        Set<Tag> tags = tagService.getAllTags();
        model.addAttribute("beer", new BeerDto());
        model.addAttribute("styles", styles);
        model.addAttribute("breweries", breweries);
        model.addAttribute("tags", tags);
        return "beer";
    }

    @PostMapping("/beers/new")
    public String createBeer(@Valid @ModelAttribute BeerDto beer,
                             @ModelAttribute Style style,
                             @RequestParam(name = "beerPicture") MultipartFile picture,
                             Principal principal,
                             Model model) {
        try {
            Beer newBeer = mapper.fromDto(beer);
            newBeer.setUser(userService.getUserByUserName(principal.getName()));
            beerService.createBeer(newBeer);
            beerService.addPictureToBeer(picture, newBeer.getId());
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            model.addAttribute("beer", beer);
            return showNewBeerForm(model);
        }
        return "redirect:/beers";
    }

    @GetMapping("/styles")
    public String showStyles(Model model) {
        model.addAttribute("styles", styleService.getAllStyles());
        return "styles";
    }

    @GetMapping("/beers/edit/{id}")
    public String showNewBeerFormView(@PathVariable int id, Model model) {
        List<Style> styles = styleService.getAllStyles();
        List<Brewery> breweries = breweryService.getBreweries();
        Set<Tag> tags = tagService.getAllTags();
        Beer beer = beerService.getBeerById(id);
        model.addAttribute("beer", beer);
        model.addAttribute("styles", styles);
        model.addAttribute("breweries", breweries);
        model.addAttribute("tags", tags);
        return "editBeer";
    }

    @PostMapping("/beers/edit/{id}")
    public String editBeer(@PathVariable int id,
                           @ModelAttribute BeerUpdateDTO beerUpdateDTO,
                           @ModelAttribute Style style,
                           @RequestParam(name = "beerPicture") MultipartFile picture) {
        Beer updateBeer = mapper.fromBeerUpdateDto(id, beerUpdateDTO);
        beerService.updateBeer(id, updateBeer);
        beerService.addPictureToBeer(picture, updateBeer.getId());
        return "redirect:/user";
    }

    @GetMapping("/beers/rate/{id}")
    public String showRateBeerFormView(@PathVariable int id, Model model) {
        Beer beer = beerService.getBeerById(id);
        Ratings rating = new Ratings();
        model.addAttribute("beer", beer);
        model.addAttribute("newRating", rating);
        return "rateBeer";
    }

    @PostMapping("/beers/rate/{beerid}")
    public String rateBeer(@PathVariable int beerid, Principal principal, @ModelAttribute("newRating") Ratings rating)
            throws CantRateBeerException, RatingOutOfRangeException {
        Beer beer = beerService.getBeerById(beerid);
        User user = userService.getUserByUserName(principal.getName());
        int userId = user.getUserId();
        int newRating2 = (int) rating.getRating();
        beerService.rateBeer(beer.getName(), userId, newRating2);

        return "redirect:/user";
    }

    @RequestMapping("/beers/addToWishlist")
    public String addBeerToWishList(@RequestParam(name = "beerID") int beerID,
                                    Principal principal) {
        try {
            User user = userService.getUserByUserName(principal.getName());
            userService.addBeerToWishList(user.getUserId(), beerID);
        } catch (DuplicateEntityException e) {
            return "redirect:/beers";
        }
        return "redirect:/beers";
    }

    @RequestMapping("/beers/addToDrinklist")
    public String addBeerToDrinkList(@RequestParam(name = "beerID") int beerID,
                                     Principal principal) {
        try {
            User user = userService.getUserByUserName(principal.getName());
            userService.addBeerToDrinkList(user.getUserId(), beerID);
        } catch (DuplicateEntityException e) {
            return "redirect:/beers";
        }
        return "redirect:/beers";
    }

    @RequestMapping("/beers/deleteBeer/{id}")
    public String delereBeer(@PathVariable(name = "id") int id,
                             Principal principal) {
        User user = userService.getUserByUserName(principal.getName());
        Beer beer = beerService.getBeerById(id);
        if (user.getUserId() == beer.getUser().getUserId()) {
            beerService.deleteBeer(id);
        }
        return "redirect:/user";
    }
}