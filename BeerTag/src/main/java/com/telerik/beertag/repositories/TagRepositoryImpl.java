package com.telerik.beertag.repositories;

import com.telerik.beertag.exceptions.EntityNotFoundException;
import com.telerik.beertag.models.Tag;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.Set;

@Repository
@Qualifier("mariadb")
public class TagRepositoryImpl implements TagRepository {

    SessionFactory sessionFactory;

    @Autowired
    public TagRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Tag addTag(Tag newTag) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(newTag);
            session.getTransaction().commit();
        }
        return newTag;
    }

    @Override
    public void removeTag(int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Tag tag = session.load(Tag.class, id);
            session.delete(tag);
            session.getTransaction().commit();
        }
    }

    @Override
    public Tag updateTag(int id, Tag newTag) {
        try (Session session = sessionFactory.openSession()) {
            Tag tag = session.load(Tag.class, id);
            if (tag == null) {
                throw new EntityNotFoundException("Tag", id);
            }
            tag.setName(newTag.getName());
            session.update(tag);
            session.getTransaction().commit();
        }
        return newTag;
    }

    @Override
    public Set<Tag> getAllTags() {
        Set<Tag> tags;
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag", Tag.class);
            tags = new HashSet<>(query.list());
        }
        return tags;
    }

    @Override
    public Tag getTagById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Tag tag = session.get(Tag.class, id);
            if (tag == null) {
                throw new EntityNotFoundException("Tag", id);
            }
            return tag;
        }
    }

    @Override
    public Tag getTagByName(String tag) {
        try(Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag where name = :tag", Tag.class);
            query.setParameter("tag", tag);
            return query.list()
                    .stream()
                    .findFirst()
                    .orElse(null);
        }
    }

    @Override
    public boolean checkIfTagExists(int id) {
        return getAllTags().contains(getTagById(id));
    }
}
