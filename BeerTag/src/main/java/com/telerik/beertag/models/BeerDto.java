package com.telerik.beertag.models;

import javax.validation.constraints.*;

import java.util.List;

import static com.telerik.beertag.Constants.*;

public class BeerDto {
    @PositiveOrZero(message = BEER_ID_ERROR_MESSAGE)
    private int id;

    @NotBlank
    @NotNull
    @Size(min = BEER_MIN_NAME_LENGTH, max = BEER_MAX_NAME_LENGTH,
            message = "Beer's name length should be between 2 and 25 symbols")
    private String name;

    @PositiveOrZero(message = BEER_STYLE_ID_ERROR_MESSAGE)
    private int styleId;

    @PositiveOrZero
    private int countryId;

    @PositiveOrZero
    private int breweryId;

    @PositiveOrZero(message = BEER_ABV_ERROR_MESSAGE)
    private double abv;

    @PositiveOrZero
    private int userId;

    @NotBlank
    @NotNull
    @Size(min = BEER_MIN_DESCRIPTION_LENGTH, max = BEER_MAX_DESCRIPTION_LENGTH,
            message = "Beer's description length should be between 10 and 150 symbols")
    private String description;

    private String picture;

    private List<Integer> tags;


    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }


    public List<Integer> getTags() {
        return tags;
    }

    public void setTags(List<Integer> tags) {
        this.tags = tags;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public int getBreweryId() {
        return breweryId;
    }

    public void setBreweryId(int breweryId) {
        this.breweryId = breweryId;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStyleId() {
        return styleId;
    }

    public void setStyleId(int styleId) {
        this.styleId = styleId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }
}
