package com.telerik.beertag.services;

import com.telerik.beertag.exceptions.DuplicateEntityException;
import com.telerik.beertag.exceptions.EntityNotFoundException;
import com.telerik.beertag.models.Beer;
import com.telerik.beertag.models.User;
import com.telerik.beertag.repositories.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import static com.telerik.beertag.Factory.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {

    @Mock
    UserRepository mockUserRepository;

    @Mock
    BeerService mockBeerService;

    @InjectMocks
    UserServiceImpl mockUserService;


    @Test
    public void createUserShouldThrow_When_UserNameAlreadyExists() {
        //Arrange
        Mockito.when(mockUserRepository.checkIfUsernameExists(createUser().getUsername()))
                .thenReturn(true);

        //Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockUserService.createUser(createUserDto()));
    }

    @Test
    public void createUserShouldThrow_When_UserEmailAlreadyExists() {
        //Arrange
        Mockito.when(mockUserRepository.checkIfUserEmailExists(anyString()))
                .thenReturn(true);

        //Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockUserService.createUser(createUserDto()));
    }

    @Test
    public void getAllUsersShould_CallRepository() {
        //Act
        mockUserService.getAllUsers();

        //Assert
        Mockito.verify(mockUserRepository,
                times(1)).getAllUsers();
    }

    @Test
    public void getAllUsersShould_ReturnEmptyList_When_NoUsers() {
        //Arrange
        Mockito.when(mockUserRepository.getAllUsers()).thenReturn(new ArrayList<>());

        //Assert
        Assert.assertTrue(mockUserService.getAllUsers().isEmpty());
    }

    @Test
    public void getAllUsersShould_ReturnAllUsers() {
        //Arrange
        List<User> expectedList = new ArrayList();
        expectedList.add(createUser());
        Mockito.when(mockUserRepository.getAllUsers()).thenReturn(expectedList);

        //Act
        List<User> actualList = mockUserService.getAllUsers();

        //Assert
        Assert.assertEquals(expectedList, actualList);
    }

    @Test
    public void getUserByIdShould_CallRepository() {
        //Arrange
        Mockito.when(mockUserRepository.checkIfUserIdExists(anyInt()))
                .thenReturn(true);

        //Act
        mockUserService.getUserById(1);

        //Assert
        Mockito.verify(mockUserRepository,
                times(1)).getUserById(anyInt());
    }

    @Test
    public void getUserByIdShouldThrow_WhenUserDoestNotExist() {
        //Arrange
        Mockito.when(mockUserRepository.checkIfUserIdExists(anyInt())).thenReturn(false);

        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockUserService.getUserById(anyInt()));
    }

    @Test
    public void getUserByIdShould_ReturnUser_WhenUserExists() {
        //Arrange
        User expectedUser = createUser();
        Mockito.when(mockUserRepository.checkIfUserIdExists(anyInt()))
                .thenReturn(true);
        Mockito.when(mockUserRepository.getUserById(anyInt()))
                .thenReturn(expectedUser);

        //Act
        User returnedUser = mockUserService.getUserById(1);

        //Assert
        Assert.assertSame(expectedUser, returnedUser);
    }

    @Test
    public void getUserByUserNameShould_CallRepository() {
        //Arrange
        Mockito.when(mockUserRepository.checkIfUsernameExists(anyString()))
                .thenReturn(true);

        //Act
        mockUserService.getUserByUserName(anyString());

        //Assert
        Mockito.verify(mockUserRepository,
                times(1)).getUserByUsername(anyString());
    }

    @Test
    public void getUserByUserNameShouldThrow_WhenUserDoestNotExist() {
        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockUserService.getUserById(anyInt()));
    }

    @Test
    public void getUserByUserNameShould_ReturnUser_WhenUserExists() {
        //Arrange
        User expectedUser = createUser();
        Mockito.when(mockUserRepository.checkIfUsernameExists(anyString()))
                .thenReturn(true);
        Mockito.when(mockUserRepository.getUserByUsername(anyString()))
                .thenReturn(expectedUser);

        //Act
        User returnedUser = mockUserService.getUserByUserName(anyString());

        //Assert
        Assert.assertSame(expectedUser, returnedUser);
    }

    @Test
    public void updateUserShould_CallRepository() {
        //Arrange
        Mockito.when(mockUserRepository.checkIfUserIdExists(anyInt())).thenReturn(true);

        //Act
        mockUserService.updateUser(createUser());

        //Assert
        Mockito.verify(mockUserRepository, times(1))
                .updateUser(any(User.class));
    }

    @Test
    public void updateUserShouldThrow_WhenUserDoesNotExist() {
        //Arrange
        Mockito.when(mockUserRepository.checkIfUserIdExists(anyInt())).thenReturn(false);

        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockUserService.updateUser(createUser()));
    }

    @Test
    public void updateUserShould_ReturnUpdatedUserName() {
        //Arrange
        User expectedUser = createUser();
        Mockito.when(mockUserRepository.checkIfUserIdExists(anyInt()))
                .thenReturn(true);
        Mockito.when(mockUserRepository.updateUser(any(User.class)))
                .thenReturn(expectedUser);

        //Act
        User returnedUser = mockUserService.updateUser(expectedUser);

        //Assert
        Assert.assertSame(expectedUser, returnedUser);
    }

    @Test
    public void updateUserDetailsShould_CallRepository() {
        //Arrange
        User user = createUser();
        Mockito.when(mockUserRepository.checkIfUserIdExists(anyInt())).thenReturn(true);
        Mockito.when(mockUserRepository.updateUser(user)).thenReturn(user);

        //Act
        mockUserService.updateUser(user);

        //Assert
        Mockito.verify(mockUserRepository, times(1))
                .updateUser(any(User.class));
    }

    @Test
    public void updateUserDetailsShould_ThrowIfEmailAlreadyExists() {
        //Arrange
        Mockito.when(mockUserRepository.checkIfUserEmailExists(anyString())).thenReturn(true);

        //Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockUserService.updateUserDetails(createUser(),
                        "Name", "Name", "email"));
    }

    @Test
    public void updateUserDetailsShould_UpdateUserDetails() {
        //Arrange
        User expectedUser = createUser();
        Mockito.when(mockUserRepository.updateUser(any(User.class))).thenReturn(expectedUser);

        //Act
        User actualUser = expectedUser;
        mockUserService.updateUserDetails(actualUser,
                "Maurice", "Laas", "email");

        //Assert
        Assert.assertSame(expectedUser, actualUser);
    }

    @Test
    public void deleteUserShould_CallRepository() {
        //Arrange
        Mockito.when(mockUserRepository.checkIfUserIdExists(anyInt())).thenReturn(true);

        //Act
        mockUserService.deleteUser(anyInt());

        //Assert
        Mockito.verify(mockUserRepository, times(1)).deleteUser(anyInt());
    }

    @Test
    public void deleteUserShouldThrow_When_UserDoesNotExist() {
        //Arrange
        Mockito.when(mockUserRepository.checkIfUserIdExists(anyInt())).thenReturn(false);

        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockUserService.deleteUser(anyInt()));
    }

    @Test
    public void deleteUserShould_ReturnDeletedUser() {
        //Arrange
        User expectedUser = createUser();
        Mockito.when(mockUserRepository.checkIfUserIdExists(anyInt())).thenReturn(true);
        Mockito.when(mockUserRepository.deleteUser(anyInt())).thenReturn(expectedUser);

        //Act
        User deletedUser = mockUserService.deleteUser(anyInt());

        //Assert
        Assert.assertSame(expectedUser, deletedUser);
    }

    @Test
    public void getUserDrinkListShouldThrow_WhenUserDoesNotExist() {
        //Arrange
        Mockito.when(mockUserRepository.checkIfUserIdExists(anyInt())).thenReturn(false);

        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockUserService.getUserDrinkList(anyInt()));
    }

    @Test
    public void getUserDrinkListShould_ReturnUserDrinkList() {
        //Arrange
        User user = createUser();
        Set<Beer> expectedDrinkList = new HashSet<>(Collections.emptySet());
        expectedDrinkList.add(createBeer());
        user.setDrinkList(expectedDrinkList);
        Mockito.when(mockUserRepository.checkIfUserIdExists(anyInt())).thenReturn(true);
        Mockito.when(mockUserService.getUserById(anyInt())).thenReturn(user);

        //Act
        Set<Beer> actualDrinkList = mockUserService.getUserDrinkList(user.getUserId());

        //Assert
        Assert.assertEquals(expectedDrinkList, actualDrinkList);
    }

    @Test
    public void addBeerToDrinkListShouldThrow_WhenUserIdDoesNotExist() {
        //Arrange
        Mockito.when(mockUserRepository.checkIfUserIdExists(anyInt())).thenReturn(false);

        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockUserService.addBeerToDrinkList(anyInt(), 1));
    }

    @Test
    public void addBeerToDrinkListShouldThrow_WhenBeerAlreadyInDrinkList() {
        //Arrange
        User user = createUser();
        Beer beer = createBeer();
        Set<Beer> drinkList = new HashSet<>();
        drinkList.add(beer);
        user.setDrinkList(drinkList);
        Mockito.when(mockUserRepository.getUserById(user.getUserId()))
                .thenReturn(user);
        Mockito.when(mockBeerService.getBeerById(beer.getId()))
                .thenReturn(beer);
        Mockito.when(mockUserRepository.checkIfUserIdExists(anyInt())).thenReturn(true);

        //Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockUserService.addBeerToDrinkList(user.getUserId(), beer.getId()));
    }

    @Test
    public void getUserWishListShouldThrow_WhenUserDoesNotExist() {
        //Arrange
        Mockito.when(mockUserRepository.checkIfUserIdExists(anyInt())).thenReturn(false);

        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockUserService.getUserWishList(anyInt()));
    }

    @Test
    public void getUserWishListShould_ReturnUserDrinkList() {
        //Arrange
        User user = createUser();
        Set<Beer> expectedWishList = new HashSet<>(Collections.emptySet());
        expectedWishList.add(createBeer());
        user.setWishList(expectedWishList);
        Mockito.when(mockUserRepository.checkIfUserIdExists(anyInt())).thenReturn(true);
        Mockito.when(mockUserService.getUserById(anyInt())).thenReturn(user);

        //Act
        Set<Beer> actualWishList = mockUserService.getUserWishList(user.getUserId());

        //Assert
        Assert.assertEquals(expectedWishList, actualWishList);
    }

    @Test
    public void addBeerToWishListShouldThrow_WhenUserIdDoesNotExist() {
        //Arrange
        Mockito.when(mockUserRepository.checkIfUserIdExists(anyInt())).thenReturn(false);

        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockUserService.addBeerToWishList(anyInt(), 1));
    }

    @Test
    public void addBeerToWishListShouldThrow_WhenBeerAlreadyInWishList() {
        //Arrange
        User user = createUser();
        Beer beer = createBeer();
        Set<Beer> wishList = new HashSet<>();
        wishList.add(beer);
        user.setWishList(wishList);
        Mockito.when(mockUserRepository.getUserById(user.getUserId()))
                .thenReturn(user);
        Mockito.when(mockBeerService.getBeerById(beer.getId()))
                .thenReturn(beer);
        Mockito.when(mockUserRepository.checkIfUserIdExists(anyInt())).thenReturn(true);

        //Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockUserService.addBeerToWishList(user.getUserId(), beer.getId()));
    }

    @Test
    public void addProfilePictureShould_CallRepository() {
        //Arrange
        User user = createUser();
        MultipartFile picture = new MultipartFile() {
            @Override
            public String getName() {
                return "fileName";
            }

            @Override
            public String getOriginalFilename() {
                return "originalFileName";
            }

            @Override
            public String getContentType() {
                return "png";
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public long getSize() {
                return 0;
            }

            @Override
            public byte[] getBytes() throws IOException {
                return "Picture Bytes".getBytes();
            }

            @Override
            public InputStream getInputStream() throws IOException {
                return null;
            }

            @Override
            public void transferTo(File dest) throws IOException, IllegalStateException {

            }
        };
        Mockito.when(mockUserRepository.checkIfUsernameExists(anyString())).thenReturn(true);
        Mockito.when(mockUserService.getUserByUserName(user.getUsername())).thenReturn(user);

        //Act
        mockUserService.addProfilePicture(user.getUsername(), picture);

        //Assert
        Mockito.verify(mockUserRepository, times(1)).updateUser(user);
    }

    @Test
    public void addProfilePictureShould_Throw_WhenUserDoesNotExist() {
        //Arrange
        Mockito.when(mockUserRepository.checkIfUsernameExists(anyString())).thenReturn(false);

        //Assert
        Assert.assertThrows(EntityNotFoundException.class,
                () -> mockUserService.addProfilePicture(createUser().getUsername(),
                        any(MultipartFile.class)));
    }

    @Test
    public void addProfilePictureShould_AddProfilePicture() {
        //Arrange
        User user = createUser();
        MultipartFile picture = new MultipartFile() {
            @Override
            public String getName() {
                return "fileName";
            }

            @Override
            public String getOriginalFilename() {
                return "originalFileName";
            }

            @Override
            public String getContentType() {
                return "png";
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public long getSize() {
                return 0;
            }

            @Override
            public byte[] getBytes() throws IOException {
                return "Picture Bytes".getBytes();
            }

            @Override
            public InputStream getInputStream() throws IOException {
                return null;
            }

            @Override
            public void transferTo(File dest) throws IOException, IllegalStateException {

            }
        };
        Mockito.when(mockUserRepository.checkIfUsernameExists(anyString())).thenReturn(true);
        Mockito.when(mockUserService.getUserByUserName(user.getUsername())).thenReturn(user);

        //Act
        mockUserService.addProfilePicture(user.getUsername(), picture);

        //Assert
        Assert.assertNotNull(user.getAvatar());
    }

    @Test
    public void removeProfilePictureShould_CallRepository() {
        //Arrange
        User user = createUser();
        Mockito.when(mockUserRepository.checkIfUsernameExists(user.getUsername())).thenReturn(true);
        Mockito.when(mockUserService.getUserByUserName(user.getUsername())).thenReturn(user);

        //Act
        mockUserService.removeProfilePicture(user.getUsername());

        //Assert
        Mockito.verify(mockUserRepository, times(1)).updateUser(user);
    }

    @Test
    public void removeProfilePictureShould_Throw_WhenUserDoesNotExists() {
        //Arrange
        User user = createUser();
        Mockito.when(mockUserRepository.checkIfUsernameExists(user.getUsername())).thenReturn(false);

        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockUserService.removeProfilePicture(user.getUsername()));
    }

    @Test
    public void removeProfilePictureShould_RemoveProfilePicture() {
        //Arrange
        User user = createUser();
        user.setAvatar("picture");
        Mockito.when(mockUserRepository.checkIfUsernameExists(user.getUsername())).thenReturn(true);
        Mockito.when(mockUserService.getUserByUserName(user.getUsername())).thenReturn(user);
        Mockito.when(mockUserRepository.updateUser(user)).thenReturn(user);

        //Act
        mockUserService.removeProfilePicture(user.getUsername());

        //Assert
        Assert.assertNull(user.getAvatar());
    }
}