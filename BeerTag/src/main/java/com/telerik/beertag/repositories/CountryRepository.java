package com.telerik.beertag.repositories;

import com.telerik.beertag.models.Country;

import java.util.List;

public interface CountryRepository {

    Country createCountry(Country newCountry);

    Country deleteCountry(int id);

    List<Country> getCountries();

    Country getCountryById(int id);

    Country updateCountry(Country country);

    boolean checkIfCountryExists(String country);

    boolean checkIfCountryExists(int id);
}
