package com.telerik.beertag.services;

import com.telerik.beertag.models.Brewery;

import java.util.List;

public interface BreweryService {

    Brewery createBrewery(Brewery newBrewery);

    Brewery deleteBrewery(int id);

    List<Brewery> getBreweries();

    Brewery getBreweryById(int id);

    Brewery updateBrewery(Brewery brewery);
}
