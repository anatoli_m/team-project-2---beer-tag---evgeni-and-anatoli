package com.telerik.beertag.exceptions;

public class CantRateBeerException extends Exception {
    public CantRateBeerException(String message) {
        super(message);
    }
}
