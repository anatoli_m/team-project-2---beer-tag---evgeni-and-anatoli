package com.telerik.beertag.services;

import com.telerik.beertag.models.Country;

import java.util.List;

public interface CountryService {

    Country createCountry(Country newCountry);

    Country deleteCountry(int id);

    List<Country> getCountries();

    Country getCountryById(int id);

    Country updateCountry(Country country);

}
