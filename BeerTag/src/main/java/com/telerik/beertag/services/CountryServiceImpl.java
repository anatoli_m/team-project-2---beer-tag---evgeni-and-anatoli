package com.telerik.beertag.services;

import com.telerik.beertag.exceptions.DuplicateEntityException;
import com.telerik.beertag.exceptions.EntityNotFoundException;
import com.telerik.beertag.models.Country;
import com.telerik.beertag.repositories.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

    private static final String COUNTRY_ALREADY_EXISTS_EXCEPTION_MESSAGE = "Country with name '%s' already exists.";

    private CountryRepository repository;

    @Autowired
    public CountryServiceImpl(CountryRepository repository) {
        this.repository = repository;
    }

    @Override
    public Country createCountry(Country newCountry) {
        if (repository.checkIfCountryExists(newCountry.getName())) {
            throw new DuplicateEntityException(
                    String.format(COUNTRY_ALREADY_EXISTS_EXCEPTION_MESSAGE, newCountry.getName()));
        }
        return repository.createCountry(newCountry);
    }

    @Override
    public Country deleteCountry(int id) {
        throwIfCountryDoesNotExist(id);
        return repository.deleteCountry(id);
    }

    @Override
    public List<Country> getCountries() {
        return repository.getCountries();
    }

    @Override
    public Country getCountryById(int id) {
        throwIfCountryDoesNotExist(id);
        return repository.getCountryById(id);
    }

    @Override
    public Country updateCountry(Country country) {
        throwIfCountryDoesNotExist(country.getId());
        return repository.updateCountry(country);
    }

    private void throwIfCountryDoesNotExist(int id) {
        if (!repository.checkIfCountryExists(id)) {
            throw new EntityNotFoundException("country", id);
        }
    }

}
