package com.telerik.beertag.models.MiddleTable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "user_drinks")
public class Ratings {

    @EmbeddedId
    private RatingsID id;

    @Column(name = "rating")
    private double rating = 0;

    public Ratings() {
    }

    public RatingsID getId() {
        return id;
    }

    public void setId(RatingsID id) {
        this.id = id;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}
