package com.telerik.beertag.repositories;

import com.telerik.beertag.exceptions.EntityNotFoundException;
import com.telerik.beertag.models.Beer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
@Qualifier("mariadb")
public class BeerRepositoryImpl implements BeerRepository {

    SessionFactory sessionFactory;

    @Autowired
    public BeerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Beer> getAllBeers() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from Beer", Beer.class)
                    .list();
        }
    }

    @Override
    public Beer getBeerById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Beer beer = session.get(Beer.class, id);
            if (beer == null) {
                throw new EntityNotFoundException("Beer", id);
            }
            return beer;
        }
    }

    @Override
    public List<Beer> getBeerByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer where name LIKE :name", Beer.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    @Override
    public List<Beer> getBeerByStyle(int styleId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer where style.id = :styleId", Beer.class);
            query.setParameter("styleId", styleId);
            return query.list();
        }
    }

    @Override
    public List<Beer> getBeerByCountry(int countryId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery(
                    "from Beer as beer where beer.brewery.country.id= :countryID", Beer.class);
            query.setParameter("countryID", countryId);
            return query.list();
        }
    }

    @Override
    public List<Beer> filterBeerByTag(String tag) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Beer> query = session.createNativeQuery("select *\n" +
                    "from beers\n" +
                    "join beer_tag bt on beers.beer_id = bt.beer_id\n" +
                    "join tags t on bt.tag_id = t.tag_id\n" +
                    "where t.tag_name like :tag", Beer.class);
            query.setParameter("tag", "%" + tag + "%");
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("Beer with %s does not exists", "Beer");
            }
            return query.list();
        }
    }

    @Override
    public List<Beer> sortBeerBy(String condition) {
        try (Session session = sessionFactory.openSession()) {
            switch (condition.toLowerCase()) {
                case "name":
                    NativeQuery<Beer> name = session.createNativeQuery("select * from beers order by beer_name", Beer.class);
                    if (name.list().isEmpty()) {
                        throw new EntityNotFoundException("%s not found", "Beers");
                    }
                    return name.list();

                case "abv":
                    NativeQuery<Beer> abv = session.createNativeQuery("select * from beers order by beer_ABV", Beer.class);
                    if (abv.list().isEmpty()) {
                        throw new EntityNotFoundException("%s not found", "Beers");
                    }

                case "rating":
                    NativeQuery<Beer> rating = session.createNativeQuery("select * from beers order by rating", Beer.class);
                    if (rating.list().isEmpty()) {
                        throw new EntityNotFoundException("%s not found", "Beers");
                    }
                    return rating.list();
            }
            return new ArrayList<>();
        }
    }

    @Override
    public Beer createBeer(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(beer);
            session.getTransaction().commit();
        }
        return beer;
    }

    @Override
    public Beer updateBeer(int id, Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(beer);
            session.getTransaction().commit();
        }
        return beer;
    }

    @Override
    public void deleteBeer(int id) {
        try (Session session = sessionFactory.openSession()) {
            Beer beerToDelete = getBeerById(id);
            session.beginTransaction();
            beerToDelete.setIsDeleted(1);
            session.update(beerToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean checkIfBeerExists(String beer) {
        return getBeerByName(beer).size() != 0;
    }

    @Override
    public List<Beer> getBeersByUser(int userID) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer where user.id LIKE :userID", Beer.class);
            query.setParameter("userID", userID);
            return query.list();
        }
    }
}
