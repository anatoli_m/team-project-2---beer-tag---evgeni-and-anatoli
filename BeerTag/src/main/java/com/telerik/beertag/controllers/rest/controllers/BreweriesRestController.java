package com.telerik.beertag.controllers.rest.controllers;

import com.telerik.beertag.exceptions.DuplicateEntityException;
import com.telerik.beertag.exceptions.EntityNotFoundException;
import com.telerik.beertag.models.Brewery;
import com.telerik.beertag.services.BreweryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Api(description = "Operations pertaining to breweries in BeerTag")
@RestController
@RequestMapping("/api/breweries")
public class BreweriesRestController {

    private BreweryService service;

    @Autowired
    public BreweriesRestController(BreweryService service) {
        this.service = service;
    }

    @ApiOperation(value = "View a list of all breweries", response = List.class)
    @GetMapping
    public List<Brewery> getBreweries() {
        return service.getBreweries();
    }

    @ApiOperation(value = "View a brewery specified by breweryID", response = Brewery.class)
    @GetMapping("/{id}")
    public Brewery getBreweryById(@PathVariable int id) {
        try {
            return service.getBreweryById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @ApiOperation(value = "Create a new brewery", response = Brewery.class)
    @PostMapping
    public Brewery createBrewery(@RequestBody Brewery country) {
        try {
            return service.createBrewery(country);
        } catch (
                DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @ApiOperation(value = "Delete a brewery by breweryID", response = Brewery.class)
    @DeleteMapping("/{id}")
    public Brewery deleteBrewery(@PathVariable int id) {
        try {
            return service.deleteBrewery(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Update a brewery by breweryID", response = Brewery.class)
    @PutMapping("/{id}")
    public Brewery updateBrewery(@PathVariable int id, @RequestBody Brewery country) {
        try {
            return service.updateBrewery(country);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}
