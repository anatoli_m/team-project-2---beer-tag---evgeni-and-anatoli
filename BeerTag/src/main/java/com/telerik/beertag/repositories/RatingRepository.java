package com.telerik.beertag.repositories;

import com.telerik.beertag.models.Beer;
import com.telerik.beertag.models.User;

public interface RatingRepository {

    void rate (User user, Beer beer, double rating);

    double getRating(Beer beer);
}
