package com.telerik.beertag.services;

import com.telerik.beertag.exceptions.DuplicateEntityException;
import com.telerik.beertag.exceptions.EntityNotFoundException;
import com.telerik.beertag.models.Brewery;
import com.telerik.beertag.repositories.BreweryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BreweryServiceImpl implements BreweryService {

    private static final String BREWERY_ALREADY_EXISTS_EXCEPTION_MESSAGE = "Brewery with name '%s' already exists.";

    private BreweryRepository repository;

    @Autowired
    BreweryServiceImpl(BreweryRepository repository) {
        this.repository = repository;
    }

    @Override
    public Brewery createBrewery(Brewery newBrewery) {
        if (repository.checkIfBreweryExists(newBrewery.getName())) {
            throw new DuplicateEntityException(
                    String.format(BREWERY_ALREADY_EXISTS_EXCEPTION_MESSAGE, newBrewery.getName()));
        }
        return repository.createBrewery(newBrewery);
    }

    @Override
    public Brewery deleteBrewery(int id) {
        throwIfBreweryDoesNotExist(id);
        return repository.deleteBrewery(id);
    }

    @Override
    public List<Brewery> getBreweries() {
        return repository.getBreweries();
    }

    @Override
    public Brewery getBreweryById(int id) {
        throwIfBreweryDoesNotExist(id);
        return repository.getBreweryById(id);
    }

    @Override
    public Brewery updateBrewery(Brewery brewery) {
        throwIfBreweryDoesNotExist(brewery.getId());
        return repository.updateBrewery(brewery);
    }

    private void throwIfBreweryDoesNotExist(int id) {
        if (!repository.checkIfBreweryExists(id)) {
            throw new EntityNotFoundException("brewery", id);
        }
    }
}
