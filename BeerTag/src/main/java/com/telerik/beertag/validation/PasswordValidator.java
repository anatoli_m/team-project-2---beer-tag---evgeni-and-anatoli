package com.telerik.beertag.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidator
        implements ConstraintValidator<ValidPassword, String> {

    private static final int MIN_PASSWORD_LENGTH = 8;

    @Override
    public boolean isValid(String password, ConstraintValidatorContext context) {
        return password.length() >= MIN_PASSWORD_LENGTH;
    }
}