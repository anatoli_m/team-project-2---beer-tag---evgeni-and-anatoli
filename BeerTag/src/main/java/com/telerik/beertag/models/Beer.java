package com.telerik.beertag.models;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "beers")
@Where(clause = "is_deleted <> 1")
public class Beer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "beer_id")
    private int id;

    @Column(name = "beer_name")
    private String name;

    @Column(name = "description")
    private String description;


    @ManyToOne
    @JoinColumn(name = "brewery_id")
    private Brewery brewery;


    @ManyToOne
    @JoinColumn(name = "style_id")
    private Style style;

    @Column(name = "beer_ABV")
    private double abv;

    @Column(name = "picture")
    private String picture;

    @Column(name = "rating")
    private double rating;

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "beer_tag",
            joinColumns = @JoinColumn(name = "beer_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private Set<Tag> tags = new HashSet<>();

    @Column(name = "is_deleted")
    private int isDeleted;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Beer() {
    }

    public Beer(String name, String description,
                double abv, String picture) {
        this.name = name;
        this.description = description;
        this.abv = abv;
        this.picture = picture;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public double getAbv() {
        return abv;
    }

    public Style getStyle() {
        return style;
    }



    public Brewery getBrewery() {
        return brewery;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public String getPicture() {
        return picture;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public void setBrewery(Brewery brewery) {
        this.brewery = brewery;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public void addTags(Tag tags) {
        this.tags.add(tags);
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

}
