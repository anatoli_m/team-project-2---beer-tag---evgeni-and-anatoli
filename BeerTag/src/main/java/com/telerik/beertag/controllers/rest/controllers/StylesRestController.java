package com.telerik.beertag.controllers.rest.controllers;


import com.telerik.beertag.exceptions.DuplicateEntityException;
import com.telerik.beertag.exceptions.EntityNotFoundException;
import com.telerik.beertag.models.Country;
import com.telerik.beertag.models.Style;
import com.telerik.beertag.services.StyleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Api(description = "Operations pertaining to Styles in BeerTag")
@RestController
@RequestMapping("/api/styles")
public class StylesRestController {

    private StyleService styleService;

    @Autowired
    public StylesRestController(StyleService styleService) {
        this.styleService = styleService;
    }

    @ApiOperation(value = "View a list of all Styles", response = List.class)
    @GetMapping
    public List<Style> getAllStyles(@RequestParam(required = false, defaultValue = "") String name) {
        if(!name.isEmpty()) {
            return styleService.getStyleByName(name);
        }
        return styleService.getAllStyles();
    }

    @ApiOperation(value = "View a Style specified by styleID", response = Style.class)
    @GetMapping("/{id}")
    public Style getStyleById(@PathVariable int id) {
        try {
            return styleService.getStyleById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Create a new Style", response = Style.class)
    @PostMapping
    public Style createStyle(@RequestBody Style style) {
        try {
            return styleService.createStyle(style);
        } catch (
                DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @ApiOperation(value = "Delete a Style specified by styleID", response = Style.class)
    @DeleteMapping("/{id}")
    public void removeStyle(@PathVariable int id) {
        try {
            styleService.deleteStyle(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Update a Style specified by styleID", response = Style.class)
    @PutMapping("/{id}")
    public Style updateStyle(@PathVariable int id, @RequestBody Style styleToUpdate) {
        try {
            return styleService.updateStyle(id, styleToUpdate);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}
