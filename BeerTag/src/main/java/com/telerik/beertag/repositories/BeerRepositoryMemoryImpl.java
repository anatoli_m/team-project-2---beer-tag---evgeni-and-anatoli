//package com.telerik.beertag.repositories;
//
//import com.telerik.beertag.exceptions.EntityNotFoundException;
//import com.telerik.beertag.models.Beer;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.stereotype.Repository;
//
//import java.util.ArrayList;
//import java.util.List;
//
//@Repository
//@Qualifier("inMemoryRepository")
//public class BeerRepositoryMemoryImpl implements BeerRepository {
//
//    private static int nextId = 0;
//    private List<Beer> beers;
//
//    @Autowired
//    public BeerRepositoryMemoryImpl(StyleRepository styleRepository, BreweryRepository breweryRepository,
//                                    CountryRepository countryRepository, TagRepository tagRepository) {
//        beers = new ArrayList<>();
//        Beer beer = new Beer("Zagorka", "Test description 1",
//                4.5, "Photos");
//        beer.setId(BeerRepositoryMemoryImpl.nextId++);
//        beer.setStyle(styleRepository.getStyleById(0));
//        beer.setCountry(countryRepository.getCountryById(1));
//        beer.setBrewery(breweryRepository.getBreweryById(1));
//        beer.addTags(tagRepository.getTagById(1));
//        beer.addTags(tagRepository.getTagById(2));
//        beers.add(beer);
//
//        Beer beer1 = new Beer("Kamentiza", "Test description 2"
//               , 6.5, "Photos");
//        beer1.setId(BeerRepositoryMemoryImpl.nextId++);
//        beer1.setStyle(styleRepository.getStyleById(1));
//        beer1.setCountry(countryRepository.getCountryById(1));
//        beer1.setBrewery(breweryRepository.getBreweryById(1));
//        beer1.addTags(tagRepository.getTagById(1));
//        beer1.addTags(tagRepository.getTagById(2));
//        beers.add(beer1);
//
//        Beer beer2 = new Beer("Staropramen", "Test description 3"
//               , 5.5, "Photos");
//        beer2.setId(BeerRepositoryMemoryImpl.nextId++);
//        beer2.setStyle(styleRepository.getStyleById(2));
//        beer2.setCountry(countryRepository.getCountryById(1));
//        beer2.setBrewery(breweryRepository.getBreweryById(1));
//        beer2.addTags(tagRepository.getTagById(1));
//        beer2.addTags(tagRepository.getTagById(2));
//        beers.add(beer2);
//    }
//
//    @Override
//    public List<Beer> getAllBeers() {
//        return beers;
//    }
//
//    @Override
//    public Beer getBeerById(int id) {
//        return beers.stream()
//                .filter(beer -> beer.getId() == id)
//                .findFirst()
//                .orElseThrow(()-> new EntityNotFoundException("Beer", id));
//    }
//
//    @Override
//    public Beer createBeer(Beer beer) {
//        beer.setId(BeerRepositoryMemoryImpl.nextId++);
//        beers.add(beer);
//        return beer;
//    }
//
//    @Override
//    public Beer updateBeer(int id, Beer beer) {
//        Beer beerToUpdate = getBeerById(id);
//        int index = beers.indexOf(beerToUpdate);
//        beer.setId(beerToUpdate.getId());
//        beers.set(index, beer);
//        return beer;
//    }
//
//    @Override
//    public void deleteBeer(int id) {
//        Beer beerToRemove = getBeerById(id);
//        beers.remove(beerToRemove);
//    }
//
//    @Override
//    public boolean checkIfBeerExists(String beer) {
//        return beers.stream()
//                .anyMatch(b -> b.getName().equals(beer));
//    }
//}
