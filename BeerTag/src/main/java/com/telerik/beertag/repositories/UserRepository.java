package com.telerik.beertag.repositories;

import com.telerik.beertag.models.User;

import java.util.List;

public interface UserRepository {

    User createUser(User newUser);

    List<User> getAllUsers();

    User getUserById(int id);

    User getUserByUsername(String username);

    User updateUser(User user);

    User deleteUser(int id);

    boolean checkIfUserEmailExists(String email);

    boolean checkIfUsernameExists(String userName);

    boolean checkIfUserIdExists(int id);

}
