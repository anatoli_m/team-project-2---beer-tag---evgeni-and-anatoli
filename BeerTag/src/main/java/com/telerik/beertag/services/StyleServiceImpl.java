package com.telerik.beertag.services;

import com.telerik.beertag.exceptions.DuplicateEntityException;
import com.telerik.beertag.exceptions.EntityNotFoundException;
import com.telerik.beertag.models.Style;
import com.telerik.beertag.repositories.StyleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StyleServiceImpl implements StyleService {

    private StyleRepository styleRepository;

    @Autowired
    public StyleServiceImpl(StyleRepository styleRepository) {
        this.styleRepository = styleRepository;
    }


    @Override
    public Style createStyle(Style style) {
        if (styleRepository.checkIfStyleExists(style.getName())) {
            throw new DuplicateEntityException(
                    String.format("Style with name %s already exists!", style.getName()));
        }
        return styleRepository.createStyle(style);
    }

    @Override
    public Style updateStyle(int id, Style style) {
        if (!styleRepository.checkIfStyleExists(styleRepository.getStyleById(id).getName())) {
            throw new EntityNotFoundException("Style", id);
        }
        return styleRepository.updateStyle(id, style);
    }

    @Override
    public void deleteStyle(int id) {
        if (!styleRepository.checkIfStyleExists(styleRepository.getStyleById(id).getName())) {
            throw new EntityNotFoundException("Style", id);
        }
        styleRepository.deleteStyle(id);
    }

    @Override
    public List<Style> getAllStyles() {
        return styleRepository.getAllStyles();
    }

    @Override
    public Style getStyleById(int id) {
        if (!styleRepository.checkIfStyleExists(styleRepository.getStyleById(id).getName())) {
            throw new EntityNotFoundException("Style", id);
        }
        return styleRepository.getStyleById(id);
    }

    @Override
    public List<Style> getStyleByName(String name) {
        return styleRepository.getStyleByName(name);
    }
}
