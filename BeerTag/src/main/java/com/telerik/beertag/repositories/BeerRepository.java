package com.telerik.beertag.repositories;

import com.telerik.beertag.models.Beer;
import com.telerik.beertag.models.Style;

import java.util.List;

public interface BeerRepository {

    List<Beer> getAllBeers();

    Beer getBeerById(int id);

    List<Beer> getBeerByName(String name);

    List<Beer> getBeerByStyle(int styleId);

    List<Beer> getBeerByCountry(int countryId);

    List<Beer> filterBeerByTag(String tag);

    List<Beer> sortBeerBy(String condition);

    Beer createBeer(Beer beer);

    Beer updateBeer(int id, Beer beer);

    void deleteBeer(int id);

    boolean checkIfBeerExists(String beer);

    List<Beer> getBeersByUser(int userID);
}
