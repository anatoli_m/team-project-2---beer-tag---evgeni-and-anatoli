package com.telerik.beertag.services;

import com.telerik.beertag.exceptions.DuplicateEntityException;
import com.telerik.beertag.exceptions.EntityNotFoundException;
import com.telerik.beertag.exceptions.WrongPasswordException;
import com.telerik.beertag.models.*;
import com.telerik.beertag.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    private static final String USERNAME_ALREADY_EXISTS_EXCEPTION_MESSAGE =
            "The username '%s' already exists. Please use a different username";
    private static final String EMAIL_ALREADY_EXISTS_EXCEPTION_MESSAGE =
            "There is already a user with this email address. Please log in";
    private static final String BEER_ALREADY_IN_WISHLIST_EXCEPTION_MESSAGE = "Beer already added to WishList.";
    private static final String BEER_ALREADY_IN_DRINKLIST_EXCEPTION_MESSAGE = "Beer already added to DrinkList.";
    private static final String WRONG_PASSWORD_EXCEPTION = "Wrong password";

    private UserRepository repository;
    private BeerService beerService;
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository repository,
                           BeerService beerService,
                           BCryptPasswordEncoder passwordEncoder) {
        this.repository = repository;
        this.beerService = beerService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User createUser(UserDto userDto) {
        if (repository.checkIfUsernameExists(userDto.getUsername())) {
            throw new DuplicateEntityException(
                    String.format(USERNAME_ALREADY_EXISTS_EXCEPTION_MESSAGE, userDto.getUsername()));
        }
        if (repository.checkIfUserEmailExists(userDto.getEmail())) {
            throw new DuplicateEntityException(EMAIL_ALREADY_EXISTS_EXCEPTION_MESSAGE);
        }

        return repository.createUser(DtoMapper.userFromDto(userDto, passwordEncoder));
    }

    @Override
    public List<User> getAllUsers() {
        return repository.getAllUsers();
    }

    @Override
    public User getUserById(int id) {
        throwIfUserIdDoesNotExist(id);
        return repository.getUserById(id);
    }

    @Override
    public User getUserByUserName(String userName) {
        throwIfUserNameDoesNotExist(userName);
        return repository.getUserByUsername(userName);
    }

    @Override
    public User updateUser(User user) {
        throwIfUserIdDoesNotExist(user.getUserId());
        return repository.updateUser(user);
    }

    @Override
    public void updateUserDetails(User user, String firstName, String lastName, String email) {
        if (!user.getEmail().equals(email) &&
                repository.checkIfUserEmailExists(email)) {
            throw new DuplicateEntityException(EMAIL_ALREADY_EXISTS_EXCEPTION_MESSAGE);
        }
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        repository.updateUser(user);
    }

    @Override
    public User deleteUser(int id) {
        throwIfUserIdDoesNotExist(id);
        return repository.deleteUser(id);
    }

    @Override
    public void addBeerToDrinkList(int userId, int beerId) {
        User user = getUserById(userId);
        Set<Beer> drinklist = user.getDrinkList();
        Beer beer = beerService.getBeerById(beerId);
        if (drinklist.stream().anyMatch(b -> b.getId() == beerId)) {
            throw new DuplicateEntityException(BEER_ALREADY_IN_WISHLIST_EXCEPTION_MESSAGE);
        }
        drinklist.add(beer);
        removeBeerFromWishList(userId, beerId);
        repository.updateUser(user);
    }

    @Override
    public void removeBeerFromDrinkList(int userId, int beerId) {
        User user = getUserById(userId);
        Beer beer = beerService.getBeerById(beerId);
        Set<Beer> drinklist = user.getDrinkList();
        drinklist.removeIf(b -> beer.getId() == b.getId());
        repository.updateUser(user);
    }

    @Override
    public Set<Beer> getUserDrinkList(int userId) {
        User user = getUserById(userId);
        return user.getDrinkList();
    }

    @Override
    public void addBeerToWishList(int userId, int beerId) {
        User user = getUserById(userId);
        Set<Beer> wishlist = user.getWishList();
        Beer beer = beerService.getBeerById(beerId);
        if (wishlist.stream().anyMatch(b -> b.getId() == beerId)) {
            throw new DuplicateEntityException(BEER_ALREADY_IN_DRINKLIST_EXCEPTION_MESSAGE);
        }
        wishlist.add(beer);
        repository.updateUser(user);
    }

    @Override
    public void removeBeerFromWishList(int userId, int beerId) {
        User user = getUserById(userId);
        Beer beer = beerService.getBeerById(beerId);
        Set<Beer> wishlist = user.getWishList();
        wishlist.removeIf(b -> beer.getId() == b.getId());
        repository.updateUser(user);
    }

    @Override
    public Set<Beer> getUserWishList(int userId) {
        User user = getUserById(userId);
        return user.getWishList();
    }

    @Override
    public void addProfilePicture(String username, MultipartFile picture) {
        User user = getUserByUserName(username);
        try {
            byte[] pictureBytes = picture.getBytes();
            String encodedPicture = Base64.getEncoder().encodeToString(pictureBytes);
            user.setAvatar(encodedPicture);
        } catch (IOException e) {
            e.printStackTrace();
        }
        repository.updateUser(user);
    }

    @Override
    public void removeProfilePicture(String username) {
        User user = getUserByUserName(username);
        user.setAvatar(null);
        repository.updateUser(user);
    }

    @Override
    public void changeUserPassword(String username, String oldPassword, String newPassword) {
        User user = getUserByUserName(username);
        boolean oldPasswordMatched = passwordEncoder.matches(oldPassword, user.getPassword());
        if (oldPasswordMatched) {
            user.setPassword(passwordEncoder.encode(newPassword));
            repository.updateUser(user);
        } else {
            throw new WrongPasswordException(WRONG_PASSWORD_EXCEPTION);
        }
    }

    private void throwIfUserIdDoesNotExist(int id) {
        if (!repository.checkIfUserIdExists(id)) {
            throw new EntityNotFoundException("user", id);
        }
    }

    private void throwIfUserNameDoesNotExist(String username) {
        if (!repository.checkIfUsernameExists(username)) {
            throw new EntityNotFoundException("user", username);
        }
    }
}
