package com.telerik.beertag.models;

import com.telerik.beertag.validation.ValidPassword;
import com.telerik.beertag.validation.PasswordMatches;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@PasswordMatches
public class UserDto {
    @NotNull(message = "First name cannot be empty")
    @NotEmpty(message = "First name cannot be empty")
    @Size(min = 2, max = 25, message = "First name should be at least 2 symbols")
    private String firstName;

    @NotNull(message = "Last name cannot be empty")
    @NotEmpty(message = "Last name cannot be empty")
    @Size(min = 2, max = 25, message = "Last name should be at least 2 symbols")
    private String lastName;

    @NotNull(message = "Username cannot be empty")
    @NotEmpty(message = "Username name cannot be empty")
    @Size(min = 4, max = 20, message = "Username must be at lest 4 symbols")
    private String username;

    @NotNull
    @NotEmpty
    @ValidPassword(message = "Password must be at least 8 symbols")
    private String password;

    @NotNull
    @NotEmpty
    @ValidPassword(message = "Password must be at least 8 symbols")
    private String matchingPassword;

    @Email(message = "This is not a valid email address")
    @NotNull
    @NotEmpty
    private String email;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
