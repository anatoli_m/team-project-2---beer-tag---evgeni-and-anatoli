package com.telerik.beertag.services;

import com.telerik.beertag.models.Beer;
import com.telerik.beertag.models.User;
import com.telerik.beertag.models.UserDto;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Set;

public interface UserService {

    User createUser(UserDto userDto);

    List<User> getAllUsers();

    User getUserById(int id);

    User getUserByUserName(String userName);

    User updateUser(User user);

    void updateUserDetails(User user, String firstName, String lastName, String email);

    User deleteUser(int id);

    void addBeerToDrinkList(int userId, int beerId);

    void removeBeerFromDrinkList(int userId, int beerId);

    Set<Beer> getUserDrinkList(int userId);

    void addBeerToWishList(int userId, int beerId);

    void removeBeerFromWishList(int userId, int beerId);

    Set<Beer> getUserWishList(int userId);

    void addProfilePicture(String username, MultipartFile picture);

    void removeProfilePicture(String username);

    void changeUserPassword(String username, String oldPassword ,String newPassword);

}
