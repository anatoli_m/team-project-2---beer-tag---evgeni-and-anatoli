//package com.telerik.beertag.repositories;
//
//import com.telerik.beertag.exceptions.EntityNotFoundException;
//import com.telerik.beertag.models.Tag;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//
//import java.util.HashSet;
//import java.util.Set;
//
//
//
//public class TagRepositoryMemoryImpl implements TagRepository {
//
//    private static int nextId = 0;
//    private Set<Tag> tags;
//
//    @Autowired
//    public TagRepositoryMemoryImpl() {
//        tags = new HashSet<>();
//        Tag tag = new Tag("Bitter");
//        tag.setTagId(TagRepositoryMemoryImpl.nextId++);
//        tags.add(tag);
//
//        tag = new Tag("Sweet");
//        tag.setTagId(TagRepositoryMemoryImpl.nextId++);
//        tags.add(tag);
//
//        tag = new Tag("Cold");
//        tag.setTagId(TagRepositoryMemoryImpl.nextId++);
//        tags.add(tag);
//
//        tag = new Tag("Tasty");
//        tag.setTagId(TagRepositoryMemoryImpl.nextId++);
//        tags.add(tag);
//
//    }
//
//    @Override
//    public Tag addTag(Tag newTag) {
//        newTag.setTagId(nextId++);
//        tags.add(newTag);
//        return newTag;
//    }
//
//    @Override
//    public void removeTag(int id) {
//
//    }
//
//    @Override
//    public Tag updateTag(int id, Tag newTag) {
//        return null;
//    }
//
//    @Override
//    public void removeTag(Tag tagToRemove) {
//        tags.remove(tagToRemove);
//    }
//
//    @Override
//    public Tag updateTag(Tag tagToUpdate) {
//        Tag newTag = tags.iterator().next();
//        newTag.setName(tagToUpdate.getName());
//        return newTag;
//    }
//
//    @Override
//    public Set<Tag> getAllTags() {
//        return tags;
//    }
//
//    @Override
//    public Tag getTagById(int id) {
//        return tags.stream()
//                .filter(tag -> tag.getTagId() == id)
//                .findFirst()
//                .orElseThrow(() -> new EntityNotFoundException("Tag",id));
//    }
//
//    @Override
//    public Tag getTagByName(String tag) {
//        return tags.stream()
//                .filter(t -> t.getName().equals(tag))
//                .findFirst()
//                .orElseThrow(() -> new EntityNotFoundException("Tag", tags.hashCode()));
//    }
//}
