package com.telerik.beertag.models;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.util.List;

import static com.telerik.beertag.Constants.*;

public class BeerUpdateDTO {

    @PositiveOrZero(message = BEER_ID_ERROR_MESSAGE)
    private int id;

    @PositiveOrZero(message = BEER_ABV_ERROR_MESSAGE)
    private double abv;

    @NotBlank
    @NotNull
    @Size(min = BEER_MIN_DESCRIPTION_LENGTH, max = BEER_MAX_DESCRIPTION_LENGTH,
            message = "Beer's description length should be between 10 and 150 symbols")
    private String description;

    private String picture;

    private List<Integer> tags;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public List<Integer> getTags() {
        return tags;
    }

    public void setTags(List<Integer> tags) {
        this.tags = tags;
    }
}
