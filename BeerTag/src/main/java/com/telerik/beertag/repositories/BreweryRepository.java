package com.telerik.beertag.repositories;

import com.telerik.beertag.models.Brewery;

import java.util.List;

public interface BreweryRepository {

    Brewery createBrewery(Brewery newBrewery);

    Brewery deleteBrewery(int id);

    List<Brewery> getBreweries();

    Brewery getBreweryById(int id);

    Brewery updateBrewery(Brewery brewery);

    boolean checkIfBreweryExists(String brewery);

    boolean checkIfBreweryExists(int id);
}
