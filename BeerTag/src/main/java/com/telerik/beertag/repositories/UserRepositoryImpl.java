package com.telerik.beertag.repositories;

import com.telerik.beertag.models.Beer;
import com.telerik.beertag.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.sql.Blob;
import java.util.List;
import java.util.Set;

@Repository
@Qualifier("mariadb")
public class UserRepositoryImpl implements UserRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public User createUser(User newUser) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(newUser);
            session.getTransaction().commit();
        }
        return newUser;
    }

    @Override
    public List<User> getAllUsers() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("FROM User", User.class)
                    .list();
        }
    }

    @Override
    public User getUserById(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(User.class, id);
        }
    }

    @Override
    public User getUserByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            String hql = String.format("FROM User WHERE username = '%s'", username);
            Query<User> query = session.createQuery(hql);
            return query.list().get(0);
        }
    }

    @Override
    public User updateUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
        return user;
    }

    @Override
    public User deleteUser(int id) {
        User user = getUserById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(user);
            session.getTransaction().commit();
        }
        return user;
    }

    @Override
    public boolean checkIfUsernameExists(String username) {
        try (Session session = sessionFactory.openSession()) {
            String hql = String.format("FROM User AS u WHERE u.username = '%s'", username);
            return !session.createQuery(hql).list().isEmpty();
        }
    }

    @Override
    public boolean checkIfUserIdExists(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            return user != null;
        }
    }

    @Override
    public boolean checkIfUserEmailExists(String email) {
        try (Session session = sessionFactory.openSession()) {
            String hql = String.format("FROM User AS u WHERE u.email = '%s'", email);
            return !session.createQuery(hql).list().isEmpty();
        }
    }

}
