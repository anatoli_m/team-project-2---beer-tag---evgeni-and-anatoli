package com.telerik.beertag.repositories;

import com.telerik.beertag.exceptions.EntityNotFoundException;
import com.telerik.beertag.models.Style;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StyleRepositoryImpl implements StyleRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public StyleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public Style createStyle(Style style) {
        try (Session session = sessionFactory.openSession()) {
            session.save(style);
        }
        return style;
    }

    @Override
    public Style updateStyle(int id, Style style) {
        try (Session session = sessionFactory.openSession()) {
            Style styleToUpdate = session.load(Style.class, id);
            if (styleToUpdate == null) {
                throw new EntityNotFoundException("Style", id);
            }
            styleToUpdate.setName(style.getName());
            session.update(styleToUpdate);
            session.getTransaction().commit();
            return styleToUpdate;
        }
    }

    @Override
    public void deleteStyle(int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Style style = session.load(Style.class, id);
            if (style == null) {
                throw new EntityNotFoundException("Style", id);
            }
            session.delete(style);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Style> getAllStyles() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("FROM Style ", Style.class)
                    .list();
        }
    }

    @Override
    public Style getStyleById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Style style = session.get(Style.class, id);
            if (style == null) {
                throw new EntityNotFoundException("Style", id);
            }
            return style;
        }
    }

    @Override
    public List<Style> getStyleByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Style> query = session.createQuery("from Style where name LIKE :name", Style.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    @Override
    public boolean checkIfStyleExists(String style) {
        return getStyleByName(style).size() != 0;
    }
}
