package com.telerik.beertag.controllers.rest.controllers;

import com.telerik.beertag.exceptions.DuplicateEntityException;
import com.telerik.beertag.exceptions.EntityNotFoundException;
import com.telerik.beertag.models.Brewery;
import com.telerik.beertag.models.Country;
import com.telerik.beertag.services.CountryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Api(description = "Operations pertaining to Countries in BeerTag")
@RestController
@RequestMapping("/api/countries")
public class CountriesRestController {

    private CountryService service;

    @Autowired
    public CountriesRestController(CountryService service) {
        this.service = service;
    }

    @ApiOperation(value = "View a list of all countries", response = List.class)
    @GetMapping
    public List<Country> getCountries() {
        return service.getCountries();
    }

    @ApiOperation(value = "View a country specified by breweryID", response = Country.class)
    @GetMapping("/{id}")
    public Country getCountryById(@PathVariable int id) {
        try {
            return service.getCountryById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @ApiOperation(value = "Create a new country", response = Country.class)
    @PostMapping
    public Country createCountry(@RequestBody Country country) {
        try {
            return service.createCountry(country);
        } catch (
                DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @ApiOperation(value = "Delete a country by countryID", response = Country.class)
    @DeleteMapping("/{id}")
    public Country deleteCountry(@PathVariable int id) {
        try {
            return service.deleteCountry(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Update a country specified by countryID", response = Country.class)
    @PutMapping
    public Country updateCountry(@RequestBody Country country) {
        try {
            return service.updateCountry(country);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}
