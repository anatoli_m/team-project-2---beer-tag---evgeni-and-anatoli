package com.telerik.beertag.services;

import com.telerik.beertag.exceptions.CantRateBeerException;
import com.telerik.beertag.exceptions.RatingOutOfRangeException;
import com.telerik.beertag.models.Beer;
import com.telerik.beertag.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface BeerService {

    List<Beer> getAllBeers();

    Page<Beer> getBeersPaged(Pageable pageable, String sortBy);

    Beer getBeerById(int id);

    List<Beer> getBeerByName(String name);

    List<Beer> getBeerByStyle(int styleId);

    List<Beer> getBeerByCountry(int countryId);

    List<Beer> filterBeerByTag(String tag);

    List<Beer> sortBeerBy(String condition);

    Beer createBeer(Beer beer);

    Beer updateBeer(int id, Beer beer);

    void deleteBeer(int id);

    void rateBeer(String beerName, int userId, double rating) throws CantRateBeerException, RatingOutOfRangeException;

    void addPictureToBeer(MultipartFile picture, int beerId);

    void removeBeerPicture(int beerId);

    List<Beer> getBeersByUser(int userID);
}
