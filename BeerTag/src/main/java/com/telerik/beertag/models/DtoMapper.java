package com.telerik.beertag.models;

import com.telerik.beertag.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class DtoMapper {

    private StyleService styleService;
    private BreweryService breweryService;
    private TagService tagService;
    private UserService userService;
    private BeerService beerService;

    @Autowired
    public DtoMapper(StyleService styleService,
                     BreweryService breweryService,
                     TagService tagService,
                     UserService userService,
                     BeerService beerService) {
        this.styleService = styleService;
        this.breweryService = breweryService;
        this.tagService = tagService;
        this.userService = userService;
        this.beerService = beerService;
    }

    public static User userFromDto(UserDto userDto, BCryptPasswordEncoder passwordEncoder) {
        Role role = new Role();
        role.setId(2);
        role.setRole("ROLE_USER");
        Set<Role> roles = new HashSet<>();
        roles.add(role);

        User user = new User();
        user.setUserId(0);
        user.setUsername(userDto.getUsername());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setAvatar(null);
        user.setDrinkList(null);
        user.setWishList(null);
        user.setRoles(roles);
        user.setDeleted(false);
        return user;
    }

    public Beer fromDto(BeerDto beerDto) {
        Beer beer = new Beer(beerDto.getName(), beerDto.getDescription(),
                beerDto.getAbv(), beerDto.getPicture());
        beer.setId(beerDto.getId());
        Style style = styleService.getStyleById(beerDto.getStyleId());
        beer.setStyle(style);
        Brewery brewery = breweryService.getBreweryById(beerDto.getBreweryId());
        beer.setBrewery(brewery);

        List<Integer> tags = beerDto.getTags();

        for (Integer tag : tags) {
            Tag tagToAdd = tagService.getTagById(tag);
            beer.addTags(tagToAdd);
        }

        return beer;
    }

    public Beer fromBeerUpdateDto(int id, BeerUpdateDTO beerUpdateDTO) {
        Beer beer = beerService.getBeerById(id);

        beer.setDescription(beerUpdateDTO.getDescription());
        beer.setAbv(beerUpdateDTO.getAbv());

        List<Integer> tags = beerUpdateDTO.getTags();

        for (Integer tag : tags) {
            Tag tagToAdd = tagService.getTagById(tag);
            if(!beer.getTags().contains(tagToAdd)){
                beer.addTags(tagToAdd);
            }
        }

        return beer;
    }

    public Beer fromDto(BeerDto beerDto, User user) {
        Beer beer = new Beer(beerDto.getName(), beerDto.getDescription(),
                beerDto.getAbv(), beerDto.getPicture());
        Style style = styleService.getStyleById(beerDto.getStyleId());
        beer.setStyle(style);
        Brewery brewery = breweryService.getBreweryById(beerDto.getBreweryId());
        beer.setBrewery(brewery);

        List<Integer> tags = beerDto.getTags();

        for (Integer tag : tags) {
            Tag tagToAdd = tagService.getTagById(tag);
            beer.addTags(tagToAdd);
        }

        beer.setUser(user);
        return beer;
    }
}
