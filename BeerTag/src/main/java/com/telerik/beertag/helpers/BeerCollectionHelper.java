package com.telerik.beertag.helpers;

import com.telerik.beertag.models.Beer;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BeerCollectionHelper {
    public static List<Beer> filterByName(List<Beer> beers, String name) {
        if (!name.isEmpty()) {
            beers = beers.stream()
                    .filter(beer -> beer.getName().toLowerCase().contains(name.toLowerCase()))
                    .collect(Collectors.toList());
        }

        return beers;
    }

    public static List<Beer> filterByStyle(List<Beer> beers, String style) {
        if (!style.isEmpty()) {
            beers = beers.stream()
                    .filter(beer -> beer.getStyle().getName().toLowerCase().contains(style.toLowerCase()))
                    .collect(Collectors.toList());
        }
        return beers;
    }

//    public static List<Beer> filterByCountry(List<Beer> beers, String country) {
//        if (!country.isEmpty()) {
//            beers = beers.stream()
//                    .filter(beer -> beer.getCountry().getName().toLowerCase().contains(country.toLowerCase()))
//                    .collect(Collectors.toList());
//        }
//        return beers;
//    }

    public static void sortBy(List<Beer> beers, String sortBy) {
        if(!sortBy.isEmpty()) {
            switch (sortBy) {
                case "name":
                    beers.sort(Comparator.comparing(Beer::getName));
                    break;
                case "style":
                    beers.sort(Comparator.comparing(beer -> beer.getStyle().getName()));
                case "abv":
                    beers.sort(Comparator.comparing(Beer::getAbv));
            }
        }
    }
}
