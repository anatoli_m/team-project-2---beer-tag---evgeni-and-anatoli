package com.telerik.beertag.services;

import com.telerik.beertag.exceptions.DuplicateEntityException;
import com.telerik.beertag.exceptions.EntityNotFoundException;
import com.telerik.beertag.models.Style;
import com.telerik.beertag.repositories.StyleRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static com.telerik.beertag.Factory.createStyle;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class StyleServiceImplTests {

    @Mock
    StyleRepository mockRepository;

    @InjectMocks
    StyleServiceImpl service;

    @Test
    public void createStyle_ShouldReturnStyle() {
        //Arrange
        Style styleToTest = createStyle();

        Mockito.when(mockRepository.createStyle(any(Style.class)))
                .thenReturn(styleToTest);

        //Act

        Style result = service.createStyle(createStyle());

        //Assert

        Assert.assertEquals(styleToTest.getName(), result.getName());

    }

    @Test
    public void createShouldThrow_When_StyleAlreadyExists() {
        //Arrange
        Mockito.when(mockRepository.checkIfStyleExists(anyString()))
                .thenReturn(true);

        //Act
        //Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.createStyle(createStyle()));

    }

    @Test
    public void updateStyle_Throws_When_Style_Does_Not_Exists() {
        //Arrange

        Style styleToUpdate = createStyle();
        Mockito.when(mockRepository.checkIfStyleExists(anyString()))
                .thenReturn(false);
        Mockito.when(mockRepository.getStyleById(anyInt()))
                .thenReturn(styleToUpdate);

        //Act
        //Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> service.updateStyle(anyInt(), styleToUpdate));
    }

    @Test
    public void updateStyle_Should_Update_Style() {
        //Arrange

        Style styleToUpdate = createStyle();
        Mockito.when(mockRepository.checkIfStyleExists(anyString()))
                .thenReturn(true);
        Mockito.when(mockRepository.getStyleById(anyInt()))
                .thenReturn(styleToUpdate);

        //Act

        service.updateStyle(anyInt(), styleToUpdate);
        //Assert
        Mockito.verify(mockRepository, times(1)).updateStyle(anyInt(), any(Style.class));
    }

    @Test
    public void deleteStyle_ShouldDeleteStyle() throws EntityNotFoundException {
        //Arrange
        Style styleToDelete = createStyle();
        Mockito.when(mockRepository.checkIfStyleExists(anyString()))
                .thenReturn(true);
        Mockito.when(mockRepository.getStyleById(0))
                .thenReturn(styleToDelete);

        service.deleteStyle(0);

        //Act

        //Assert
        Mockito.verify(mockRepository, times(1)).deleteStyle(0);

    }

    @Test
    public void deleteStyle_ShouldThrow_When_StyleDoesNotExists() {
        //Arrange

        Style styleToDelete = createStyle();
        Mockito.when(mockRepository.checkIfStyleExists(anyString()))
                .thenReturn(false);
        Mockito.when(mockRepository.getStyleById(anyInt()))
                .thenReturn(styleToDelete);

        //Act
        //Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> service.deleteStyle(anyInt()));

    }

    @Test
    public void getAllStyles_Should_Return_EmptyList_When_NoStyles() {
        //Arrange

        Mockito.when(mockRepository.getAllStyles())
                .thenReturn(new ArrayList<>());

        //Act

        service.getAllStyles();

        //Assert

        Assert.assertTrue(mockRepository.getAllStyles().isEmpty());
    }

    @Test
    public void getAllStyles_Should_Return_AllStyles() {
        //Arrange

        Mockito.when(mockRepository.getAllStyles())
                .thenReturn(Arrays.asList(
                        createStyle(),
                        createStyle()
                ));
        //Act

        service.getAllStyles();

        //Assert

        Assert.assertEquals(2, mockRepository.getAllStyles().size());
    }

    @Test
    public void getById_Should_ReturnStyle_When_MatchExists() {

        //Arrange
        Style styleToTest = createStyle();

        Mockito.when(mockRepository.getStyleById(0))
                .thenReturn(styleToTest);
        Mockito.when(mockRepository.checkIfStyleExists(anyString()))
                .thenReturn(true);

        //Act
        Style result = service.getStyleById(0);

        //Assert
        Assert.assertEquals("testStyle", result.getName());

    }

    @Test
    public void getByIdShould_CallRepository() {
        //Arrange
        Style styleToTest = createStyle();
        Mockito.when(mockRepository.getStyleById(anyInt()))
                .thenReturn(styleToTest);
        Mockito.when(mockRepository.checkIfStyleExists(anyString()))
                .thenReturn(true);

        //Act
        service.getStyleById(0);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(2)).getStyleById(anyInt());

    }

    @Test
    public void getByIdShould_Throw_When_Beer_Does_Not_Exists() {
        //Arrange
        Style styleToCheck = createStyle();

        Mockito.when(mockRepository.checkIfStyleExists(anyString()))
                .thenReturn(false);
        Mockito.when(mockRepository.getStyleById(anyInt()))
                .thenReturn(styleToCheck);

        //Act
        //Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> service.getStyleById(anyInt()));
    }

    @Test
    public void getStyleByName_Should_Return_Style_When_Exists() {
        //Arrange
        Style styleToFind = createStyle();
        Mockito.when(mockRepository.getStyleByName(anyString()))
                .thenReturn(Collections.singletonList(styleToFind));
        //Act

        service.getStyleByName(anyString());

        //Assert

        Assert.assertEquals(1, mockRepository.getStyleByName(anyString()).size());
    }

    @Test
    public void getStyleByName_Should_Return_EmptyList_When_Style_Does_Not_Exists() {
        //Arrange
        Style styleToFind = createStyle();
        Mockito.when(mockRepository.getStyleByName(anyString()))
                .thenReturn(new ArrayList<>());
        //Act

        service.getStyleByName(anyString());

        //Assert

        Assert.assertTrue(mockRepository.getStyleByName(anyString()).isEmpty());
    }


}
