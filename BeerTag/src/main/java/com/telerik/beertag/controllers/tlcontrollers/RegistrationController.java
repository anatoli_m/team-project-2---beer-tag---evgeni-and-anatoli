package com.telerik.beertag.controllers.tlcontrollers;

import com.telerik.beertag.exceptions.DuplicateEntityException;
import com.telerik.beertag.models.User;
import com.telerik.beertag.models.UserDto;
import com.telerik.beertag.services.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class RegistrationController {

    private UserServiceImpl userService;

    @Autowired
    public RegistrationController(UserServiceImpl userService) {
        this.userService = userService;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new UserDto());
        return "user-registration";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute("user") UserDto userDto,
                               BindingResult bindingResult,
                               Model model,
                               Errors errors) {
        try {
            if (bindingResult.hasErrors()) {
                model.addAttribute("error", bindingResult.getFieldError().getDefaultMessage());
                return "user-registration";
            }
            userService.createUser(userDto);
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "user-registration";
        }

        return "redirect:/login?success";
    }
}

