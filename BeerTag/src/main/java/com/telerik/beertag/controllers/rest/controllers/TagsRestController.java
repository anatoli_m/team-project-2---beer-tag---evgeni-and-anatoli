package com.telerik.beertag.controllers.rest.controllers;


import com.telerik.beertag.exceptions.DuplicateEntityException;
import com.telerik.beertag.exceptions.EntityNotFoundException;
import com.telerik.beertag.models.Style;
import com.telerik.beertag.models.Tag;
import com.telerik.beertag.services.TagService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Set;

@Api(description = "Operations pertaining to Tags in BeerTag")
@RestController
@RequestMapping("/api/tags")
public class TagsRestController {

    private TagService tagService;

    @Autowired
    public TagsRestController(TagService tagService) {
        this.tagService = tagService;
    }

    @ApiOperation(value = "View a list of all Tags", response = List.class)
    @GetMapping
    public Set<Tag> getAllTags() {
        return tagService.getAllTags();
    }

    @ApiOperation(value = "View a Tag specified by tagID", response = Tag.class)
    @GetMapping("/{id}")
    public Tag getTagById(@PathVariable int id) {
        try {
            return tagService.getTagById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Create a new Tag", response = Tag.class)
    @PostMapping
    public Tag addTag(@RequestBody Tag tag) {
        try {
            return tagService.addTag(tag);
        } catch (
                DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @ApiOperation(value = "Delete a Tag specified by tagID", response = Tag.class)
    @DeleteMapping("/{id}")
    public void removeTag(@PathVariable int tagToRemove) {
        try {
            tagService.removeTag(tagToRemove);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Update a Tag specified by tagID", response = Tag.class)
    @PutMapping("/{id}")
    public Tag updateTag(@PathVariable int id, @RequestBody Tag tagToUpdate) {
        try {
            return tagService.updateTag(id, tagToUpdate);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}
