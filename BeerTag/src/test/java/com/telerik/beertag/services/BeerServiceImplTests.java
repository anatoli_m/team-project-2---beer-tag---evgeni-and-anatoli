package com.telerik.beertag.services;

import com.telerik.beertag.exceptions.DuplicateEntityException;
import com.telerik.beertag.exceptions.EntityNotFoundException;
import com.telerik.beertag.exceptions.InvalidOperationException;
import com.telerik.beertag.models.Beer;
import com.telerik.beertag.models.User;
import com.telerik.beertag.repositories.BeerRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static com.telerik.beertag.Factory.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class BeerServiceImplTests {

    @Mock
    BeerRepository mockRepository;

    @InjectMocks
    BeerServiceImpl service;


    @Test
    public void getById_Should_ReturnBeer_When_MatchExists() {

        //Arrange
        Beer beerToTest = createBeer();

        Mockito.when(mockRepository.getBeerById(anyInt()))
                .thenReturn(beerToTest);

        //Act
        Beer result = service.getBeerById(2);

        //Assert
        Assert.assertEquals("Zagorka", result.getName());

    }

    @Test
    public void getByIdShould_CallRepository() {
        //Arrange
        Beer beerToTest = createBeer();
        Mockito.when(mockRepository.getBeerById(anyInt()))
                .thenReturn(beerToTest);

        //Act
        service.getBeerById(1);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getBeerById(anyInt());

    }

    @Test(expected = DuplicateEntityException.class)
    public void createShouldThrow_When_BeerAlreadyExists() {
        //Arrange
        Mockito.when(mockRepository.checkIfBeerExists(anyString()))
                .thenReturn(true);

        //Act
        //Assert
        service.createBeer(createBeer());

    }

    @Test
    public void createBeer_ShouldReturnBeer() {
        //Arrange
        Beer beerToTest = createBeer();

        Mockito.when(mockRepository.createBeer(any(Beer.class)))
                .thenReturn(beerToTest);

        //Act

        Beer result = service.createBeer(createBeer());

        //Assert

        Assert.assertEquals(beerToTest.getName(), result.getName());

    }

    @Test
    public void deleteBeer_ShouldDeleteBeer() throws EntityNotFoundException {
        //Arrange
        Beer beerToDelete = createBeer();
        Mockito.when(mockRepository.checkIfBeerExists(anyString()))
                .thenReturn(true);
        Mockito.when(mockRepository.getBeerById(0))
                .thenReturn(beerToDelete);

        service.deleteBeer(0);

        //Act

        //Assert
        Mockito.verify(mockRepository, times(1)).deleteBeer(0);

    }

    @Test(expected = EntityNotFoundException.class)
    public void deleteBeer_ShouldThrow_When_BeerDoesNotExists() {
        //Arrange

        Beer beerToDelete = createBeer();
        Mockito.when(mockRepository.checkIfBeerExists(anyString()))
                .thenReturn(false);
        Mockito.when(mockRepository.getBeerById(anyInt()))
                .thenReturn(beerToDelete);

        //Act
        //Assert
        service.deleteBeer(anyInt());

    }

    @Test
    public void getAllBeers_Should_Return_EmptyList_When_NoBeers() {
        //Arrange

        Mockito.when(mockRepository.getAllBeers())
                .thenReturn(new ArrayList<>());

        //Act

        service.getAllBeers();

        //Assert

        Assert.assertTrue(mockRepository.getAllBeers().isEmpty());
    }

    @Test
    public void getAllBeers_Should_Return_AllBeers() {
        //Arrange

        Mockito.when(mockRepository.getAllBeers())
                .thenReturn(Arrays.asList(
                        createBeer(),
                        createBeer()
                ));
        //Act

        service.getAllBeers();

        //Assert

        Assert.assertEquals(2, mockRepository.getAllBeers().size());
    }

    @Test
    public void getBeerByName_Should_Return_Beer_When_Exists() {
        //Arrange
        Beer beerToFind = createBeer();
        Mockito.when(mockRepository.getBeerByName(anyString()))
                .thenReturn(Collections.singletonList(beerToFind));
        //Act

        service.getBeerByName(anyString());

        //Assert

        Assert.assertEquals(1, mockRepository.getBeerByName(anyString()).size());
    }

    @Test
    public void getBeerByName_Should_Return_EmptyList_When_Beer_Does_Not_Exists() {
        //Arrange
        Beer beerToFind = createBeer();
        Mockito.when(mockRepository.getBeerByName(anyString()))
                .thenReturn(new ArrayList<>());
        //Act

        service.getBeerByName(anyString());

        //Assert

        Assert.assertTrue(mockRepository.getBeerByName(anyString()).isEmpty());
    }

    @Test
    public void getBeerByStyle_Should_Return_Beer_When_Exists() {
        //Arrange
        Beer beerToFind = createBeer();
        Mockito.when(mockRepository.getBeerByStyle(anyInt()))
                .thenReturn(Collections.singletonList(beerToFind));
        //Act

        service.getBeerByStyle(anyInt());

        //Assert

        Assert.assertEquals(1, mockRepository.getBeerByStyle(anyInt()).size());
    }

    @Test
    public void getBeerByStyle_Should_Return_EmptyList_When_Beer_Does_Not_Exists() {
        //Arrange
        Beer beerToFind = createBeer();
        Mockito.when(mockRepository.getBeerByStyle(anyInt()))
                .thenReturn(new ArrayList<>());
        //Act

        service.getBeerByStyle(anyInt());

        //Assert

        Assert.assertTrue(mockRepository.getBeerByStyle(anyInt()).isEmpty());
    }

    @Test(expected = EntityNotFoundException.class)
    public void updateBeer_Throws_When_Beer_Does_Not_Exists() {
        //Arrange
        User user = createUser();
        Beer beerToUpdate = createBeer();
        beerToUpdate.setUser(user);
        Mockito.when(mockRepository.checkIfBeerExists(beerToUpdate.getName()))
                .thenReturn(false);
        Mockito.when(mockRepository.getBeerById(0))
                .thenReturn(beerToUpdate);
        //Act
        //Assert
       service.updateBeer(0, beerToUpdate);
    }

    @Test
    public void updateBeer_Should_Update_Beer() {
        //Arrange
        User user = createUser();
        Beer beerToUpdate = createBeer();
        beerToUpdate.setUser(user);
        Mockito.when(mockRepository.checkIfBeerExists(anyString()))
                .thenReturn(true);
        Mockito.when(mockRepository.getBeerById(anyInt()))
                .thenReturn(beerToUpdate);
        Mockito.when(mockRepository.updateBeer(anyInt(), any(Beer.class)))
                .thenReturn(beerToUpdate);
        //Act

        service.updateBeer(anyInt(), beerToUpdate);
        //Assert
        Mockito.verify(mockRepository, times(1)).updateBeer(anyInt(), any(Beer.class));
    }

//    @Test(expected = InvalidOperationException.class)
//    public void updateBeer_Should_Throw_When_User_Has_No_Rights() {
//        //Arrange
//        User userNoRights =  createUserNoRights();
//        User user = createUser();
//        Beer beerToUpdate = createBeer();
//        beerToUpdate.setUser(user);
//        Mockito.when(mockRepository.checkIfBeerExists(beerToUpdate.getName()))
//                .thenReturn(true);
//        Mockito.when(mockRepository.getBeerById(0))
//                .thenReturn(beerToUpdate);
//        //Act
//        //Assert
//       service.updateBeer(0, beerToUpdate);
//    }

}
